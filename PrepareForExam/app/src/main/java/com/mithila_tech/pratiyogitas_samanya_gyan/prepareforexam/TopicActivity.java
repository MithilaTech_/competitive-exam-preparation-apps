package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.TopicHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Topic;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;
import com.shreyaspatil.firebase.recyclerpagination.DatabasePagingOptions;
import com.shreyaspatil.firebase.recyclerpagination.FirebaseRecyclerPagingAdapter;
import com.shreyaspatil.firebase.recyclerpagination.LoadingState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TopicActivity extends AppCompatActivity {

    private static final String TAG = TopicActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseRecyclerPagingAdapter recyclerViewAdapter;
    private DatabaseReference databaseReference,subjectreference;
    private List<Topic> allTopics;
    private String sname;
    private ProgressDialog mProgressBar;
    int count=0;
    private String topc_language;
    private Query query;
    private boolean flag,istopictestseries=false,issubjecttestseries=false,iscommontestseries=false;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DatabasePagingOptions<Topic> options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        topc_language = SharedPrefManager.getLang(TopicActivity.this);
        if(intent !=null) {
            sname = intent.getStringExtra("subjectname");
            flag = intent.getBooleanExtra("isStudyTest",true);
            istopictestseries = intent.getBooleanExtra("istopictestseries",false);
            issubjecttestseries = intent.getBooleanExtra("issubjecttestseries",false);
            iscommontestseries= intent.getBooleanExtra("iscommontestseries",false);
            if(sname ==null || sname.length()<=0) {
                sname = "भारतीय अर्थव्यवस्था";
            }
        }
        getSupportActionBar().setTitle(sname);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        allTopics= new ArrayList<Topic>();
        mProgressBar = new ProgressDialog(TopicActivity.this);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();

        databaseReference.keepSynced(true);

        databaseReference.keepSynced(false);
        subjectreference = databaseReference.child("/topics");
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(5)
                .setPageSize(10)
                .build();

        //Initialize FirebasePagingOptions
        options = new DatabasePagingOptions.Builder<Topic>()
                .setLifecycleOwner(this)
                .setQuery(subjectreference, config, Topic.class)
                .build();
        query = subjectreference.orderByChild("sname").equalTo(sname);
        recyclerView = (RecyclerView)findViewById(R.id.top_list);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(linearLayoutManager);

        mProgressBar.setIndeterminate(true);
        mProgressBar.setMessage(getString(R.string.title_loading));
        mProgressBar.show();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                HashMap<String,String> ss= (HashMap<String, String>) dataSnapshot.getValue();

                    String subname = (String) ss.get("sname");
                    String tname = (String) ss.get("tname");
                    String tlang = (String) ss.get("tlanguage");
                    Topic topic = new Topic();

                        topic.setTid(String.valueOf(count));
                        topic.setSname(subname);
                        topic.setTlanguage(tlang);
                        topic.setTname(tname);
                        allTopics.add(topic);

                }




            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(allTopics!=null && allTopics.size()>0) {
                    if(mProgressBar.isShowing()) {
                    mProgressBar.dismiss();

                }

                    fillData();

                } else {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }
                    Toast.makeText(TopicActivity.this, "No topics found...", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void fillData() {
        recyclerViewAdapter = new FirebaseRecyclerPagingAdapter<Topic,TopicHolder>(options) {
            @Override
            protected void onBindViewHolder(final @NonNull TopicHolder topicHolder, int i, final @NonNull Topic ttopic) {
                if (i < allTopics.size()) {
                    final Topic topic = allTopics.get(i);
                    if (topic != null) {
                        String firstLetter = topic.getTname();
                        if (firstLetter != null && firstLetter.length() > 0) {
                            topicHolder.categoryTitle.setText(firstLetter);
                        }

                    }


                    topicHolder.categoryTitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String taskTitle = topic.getTname();
                            if(flag==true) {
                                Intent i = new Intent(TopicActivity.this, StudyQuestionActivity.class);
                                i.putExtra("topicname", topicHolder.categoryTitle.getText().toString());
                                TopicActivity.this.startActivity(i);
                            }else if(istopictestseries) {
                                Intent i = new Intent(TopicActivity.this, AvailableTestSeries.class);
                                i.putExtra("topicname", topicHolder.categoryTitle.getText().toString());

                                i.putExtra("istopictestseries",istopictestseries);
                                i.putExtra("issubjecttestseries",issubjecttestseries);
                                i.putExtra("iscommontestseries",iscommontestseries);
                                TopicActivity.this.startActivity(i);
                            }

                        }
                    });

                }
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        // Do your loading animation
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        // Stop Animation
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        //Reached end of Data set
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        retry();
                        break;
                }
            }

            @Override
            protected void onError(@NonNull DatabaseError databaseError) {
                super.onError(databaseError);
                mSwipeRefreshLayout.setRefreshing(false);
                databaseError.toException().printStackTrace();
            }

            @Override
            public TopicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.topic_list, parent, false);
               /* final NativeExpressAdView adView = (NativeExpressAdView) view.findViewById(R.id.nativeAdView);
                adView.loadAd(new AdRequest.Builder().build());
                adView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        adView.setVisibility(View.VISIBLE);
                    }
                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                    }
                    @Override
                    public void onAdLeftApplication() {
                        super.onAdLeftApplication();
                    }
                    @Override
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        adView.setVisibility(View.GONE);
                    }
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        adView.setVisibility(View.GONE);
                    }
                });*/
                return new TopicHolder(TopicActivity.this,view);

            }

            /*@Override
            protected void populateViewHolder
                    (final TopicHolder holder, final Topic journalEntry, int position) {

            }*/
        };
      //  recyclerView.setItemViewCacheSize(allTopics.size());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(recyclerViewAdapter);
        //Set listener to SwipeRefreshLayout for refresh action
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerViewAdapter.refresh();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.stopListening();
        }
    }
}
