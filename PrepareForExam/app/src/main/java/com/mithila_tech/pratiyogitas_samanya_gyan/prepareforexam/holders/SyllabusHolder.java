package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;

/**
 * Created by ASUS on 2/21/2018.
 */

public class SyllabusHolder extends RecyclerView.ViewHolder {
    private final String TAG = com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.SyllabusHolder.class.getSimpleName();

    public TextView syllabusTitle;
    public ImageView syllabusImage;
    public SyllabusHolder(final Context context, final View itemView) {
        super(itemView);

        syllabusTitle = (TextView)itemView.findViewById(R.id.syllabusTitle);
        syllabusImage = (ImageView) itemView.findViewById(R.id.imgSyllabusImage);


    }
}
