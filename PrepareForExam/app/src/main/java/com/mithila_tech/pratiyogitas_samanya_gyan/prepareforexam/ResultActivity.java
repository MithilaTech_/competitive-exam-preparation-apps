package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.widget.LinearLayout;

import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters.ResultAdapter;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters.TestResultAdapter;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Question;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.TestSeries;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    private int totalscore;
    private ArrayList<Question> allquestions;
    private ArrayList<TestSeries> allTestSeriesquestions;
    private LinearLayout layAnswer;
    private static final String TAG = ResultActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private String tname;
    private ImageLoader imageLoader;
    private ProgressDialog mProgressBar;
    private String q_language;
    private ResultAdapter adapter;
    private TestResultAdapter testadapter;
    private TextView txtScore;
    private Intent mShareIntent;
    private ShareActionProvider mShareActionProvider;
    private boolean isTestSeries;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Result");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();

        txtScore = (TextView) findViewById(R.id.txtscore);

        recyclerView = (RecyclerView)findViewById(R.id.results_list);
        imageLoader = ImageLoader.getInstance();
        if(i!=null) {
            isTestSeries = i.getBooleanExtra("isTestSeries",false);
            totalscore = i.getIntExtra("score",0);
            txtScore.setText("Your score :"+totalscore);
            if(isTestSeries) {
                allTestSeriesquestions = i.getParcelableArrayListExtra("questionlist");
                testadapter = new TestResultAdapter(ResultActivity.this,imageLoader,allTestSeriesquestions);



            }


        }
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.scrollToPosition(0);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        if(isTestSeries)
            recyclerView.setAdapter(testadapter);

        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
