package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.SyllabusHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Syllabus;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import  com.google.android.gms.ads.AdView;
import com.shreyaspatil.firebase.recyclerpagination.DatabasePagingOptions;
import com.shreyaspatil.firebase.recyclerpagination.FirebaseRecyclerPagingAdapter;
import com.shreyaspatil.firebase.recyclerpagination.LoadingState;

public class SyllabusActivity extends AppCompatActivity {
    private LinearLayout layAnswer;
    private static final String TAG = SyllabusActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseRecyclerPagingAdapter recyclerViewAdapter;
    private DatabaseReference databaseReference,quesreference;
    private String tname;
    private ImageLoader imageLoader;
    private List<Syllabus> allSyllabus;
    private ProgressDialog mProgressBar;
    private String q_language;
    private int count=0;
    private Query query;
    private String slanguage;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DatabasePagingOptions<Syllabus> options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Syllabus");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        slanguage = SharedPrefManager.getLang(SyllabusActivity.this);
       // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.keepSynced(true);

        databaseReference.keepSynced(false);
        quesreference = databaseReference.child("/syllabus");
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(5)
                .setPageSize(10)
                .build();

        //Initialize FirebasePagingOptions
        options = new DatabasePagingOptions.Builder<Syllabus>()
                .setLifecycleOwner(this)
                .setQuery(quesreference, config, Syllabus.class)
                .build();
        query = quesreference.orderByChild("clanguage").equalTo(slanguage);
        recyclerView = (RecyclerView)findViewById(R.id.syllabus_list);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(linearLayoutManager);
        imageLoader = ImageLoader.getInstance();
        q_language = SharedPrefManager.getLang(SyllabusActivity.this);

        allSyllabus= new ArrayList<Syllabus>();
        mProgressBar = new ProgressDialog(SyllabusActivity.this);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setMessage(getString(R.string.title_loading));
        mProgressBar.setCancelable(false);
        mProgressBar.show();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //Log.e("SubjectActivity","onChildAdded()");
                HashMap<String,String> obj= (HashMap<String, String>) dataSnapshot.getValue();


                String blogurl = (String) obj.get("burl");
                String coursedetail = (String) obj.get("cdetail");
                String courselang = (String) obj.get("clanguage");
                String coursetitle = (String) obj.get("ctitle");
//                String storedate =  Long.parseLong(obj.get("syllabus_img_store_date"));
                String simgurl = (String) obj.get("syllabus_img_url");

                Syllabus syllabus = new Syllabus();
                syllabus.setBurl(blogurl);
                syllabus.setCdetail(coursedetail);
                syllabus.setClanguage(courselang);
                syllabus.setCtitle(coursetitle);
    //            syllabus.setSyllabus_img_store_date(storedate);
                syllabus.setSyllabus_img_url(simgurl);
                allSyllabus.add(syllabus);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(allSyllabus!=null && allSyllabus.size()>0) {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }

                    fillData();

                } else {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }
                    Toast.makeText(SyllabusActivity.this, "No courses found...", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


    }


    public void toggle_contents(View view) {

    }
    String ansval = "A";
    public void fillData() {
        recyclerViewAdapter = new FirebaseRecyclerPagingAdapter<Syllabus,SyllabusHolder>(options) {
            @Override
            protected void onBindViewHolder(final @NonNull SyllabusHolder syllabusHolder, int i, final @NonNull Syllabus syllabus) {
                if (i < allSyllabus.size()) {

                    final Syllabus q = allSyllabus.get(i);

                    syllabusHolder.syllabusTitle.setText(q.getCtitle());
                    if (q.getSyllabus_img_url()!=null && !q.getSyllabus_img_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        imageLoader.displayImage(q.getSyllabus_img_url(), syllabusHolder.syllabusImage);
                    }

                    syllabusHolder.syllabusTitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(SyllabusActivity.this, SyllabusBlogActivity.class);
                            i.putExtra("stitle",q.getCtitle());
                            i.putExtra("burl", q.getBurl());
                            SyllabusActivity.this.startActivity(i);
                        }
                    });
                    //}
                }
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        // Do your loading animation
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        // Stop Animation
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        //Reached end of Data set
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        retry();
                        break;
                }
            }

            @Override
            protected void onError(@NonNull DatabaseError databaseError) {
                super.onError(databaseError);
                mSwipeRefreshLayout.setRefreshing(false);
                databaseError.toException().printStackTrace();
            }

            @Override
            public SyllabusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.content_syllabus, parent, false);

                return new SyllabusHolder(SyllabusActivity.this,view);

            }

          /*  @Override
            protected void populateViewHolder
                    (final SyllabusHolder holder, final Syllabus journalEntry, int position) {

            }*/
        };
        recyclerView.setItemViewCacheSize(allSyllabus.size());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);
        //Set listener to SwipeRefreshLayout for refresh action
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerViewAdapter.refresh();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.stopListening();
        }
    }
}
