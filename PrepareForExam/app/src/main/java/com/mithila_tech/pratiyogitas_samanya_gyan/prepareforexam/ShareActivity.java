package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.widget.TextView;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.TestSeries;

import java.util.ArrayList;
import com.google.android.gms.ads.AdView;

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {


    private ArrayList<String> appList ;
    private String filePath ;
    private Context mContext;
    private int totalscore;
    private TextView txtScore,txtAttempted,txtUnAttempted,txtRank;

    private ArrayList<TestSeries> allTestquestions;
    private String type =null ;
    private int outof;
    private boolean isTestSeries;
    private int attempted,unattempted,rank;
    private int wrongans;
    private TextView txtNofoQues,txtWrongAns;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_share);
        mContext = this;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Share score");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if(intent!=null) {
            isTestSeries = intent.getBooleanExtra("isTestSeries",false);
            outof = intent.getIntExtra("outof",0);
            totalscore = intent.getIntExtra("score",0);
            attempted = intent.getIntExtra("attempted",1);
            unattempted = intent.getIntExtra("unattempted",1);
            wrongans = intent.getIntExtra("wrongans",0);
            rank = intent.getIntExtra("rank",1);
            if(isTestSeries)
                allTestquestions = intent.getParcelableArrayListExtra("questionlist");
        }
        txtNofoQues = findViewById(R.id.txtNoofQues);
        txtNofoQues.setText(String.valueOf(outof));
        txtWrongAns = findViewById(R.id.txtWrongAns);
        txtWrongAns.setText(String.valueOf(wrongans));
        txtScore = findViewById(R.id.textScoreView);
        txtScore.setText(totalscore +"/"+outof);
        txtAttempted = findViewById(R.id.txtAttempted);
        txtAttempted.setText(String.valueOf(attempted));
        txtUnAttempted = findViewById(R.id.txtUnAttempted);
        txtUnAttempted.setText(String.valueOf(unattempted));
        txtRank = findViewById(R.id.txtRank);
        txtRank.setText(String.valueOf(rank));
        findViewById(R.id.solutions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ResultActivity.class);
                i.putExtra("score",totalscore);
                i.putExtra("isTestSeries", isTestSeries);
                if(isTestSeries)
                    i.putParcelableArrayListExtra("questionlist", allTestquestions);

                ShareActivity.this.startActivity(i);
                finish();
            }
        });


        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShareActivity.this, ProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        findViewById(R.id.rateUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.adshak.ganeshframes"));
                startActivity(intent);*/
            }
        });

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    @Override
    public void onClick(View v) {

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    @Override
    public void onPause() {

        super.onPause();
    }


     @Override
    public void onResume() {
        super.onResume();



    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }




    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onShareClick(View view) {

        String shareText ="Quiz App.\n"/*+ShareActivity.this.getResources().getString(R.string.app_playstore_link) */;
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(Intent.EXTRA_SUBJECT,shareText) ;
        intent.putExtra(Intent.EXTRA_TEXT,totalscore +"/"+outof) ;
        startActivity(Intent.createChooser(intent, getResources().getText(R.string.send_to)));
    }
}
