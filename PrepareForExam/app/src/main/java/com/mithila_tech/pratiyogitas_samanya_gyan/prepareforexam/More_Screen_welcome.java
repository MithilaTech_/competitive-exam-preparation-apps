package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class More_Screen_welcome extends AppCompatActivity {

    TextView textView1,textView2;
    Button button_OfficialWebsite,
            button_newRegistration_Button,
            button_RenewOld_Registration,
            button_FindRegistrationNo,
            button_FindUserIDandPassword,
            button_TransferRegistration,
            button_RatingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_more_welcome_screen);


        //-----------------------------------------------------------------

        // my_child_toolbar is defined in the layout file
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);

        setSupportActionBar(myToolbar);
        // Home (Back) icon set code
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        // Get a support ActionBar corresponding to this toolbar
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        //-----------------------------------------------------------------

        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        button_OfficialWebsite = (Button) findViewById(R.id.AboutUs_button);
        button_newRegistration_Button = (Button) findViewById(R.id.MoreApp_Buttom);
        button_RenewOld_Registration = (Button) findViewById(R.id.SendUsYourComments_button);
        button_FindRegistrationNo = (Button) findViewById(R.id.LikeOurFacebookPage_button);
        button_FindUserIDandPassword = (Button) findViewById(R.id.JoinOurFacebookGroup_button);
        button_TransferRegistration = (Button) findViewById(R.id.SubscribeOurYouTubeChannel_button);
        button_RatingButton = findViewById(R.id.RatingButton);



        textView1.setText("MP Rojgar Panjiyan Registration Online Form - Apply Now");
        textView2.setText("The Madhya Pradesh Employment Exchange Office MP Rojgar Department has released a" +
                "official advertisement for the new candidates who are unemployed in the MP state and" +
                "want to allowance by State Government. Attracted and qualified applicants are invited for" +
                "the MP Rojgar Panjiyan Registration Online Form. candidates need to check the whole page" +
                "and can check their MP Rojgar Panjiyan eligibility criteria, online registration procedure and more information also.");
    }


    //All Button onClick Code is start
    public void onClick_Button_OfficialWebsite(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://mprojgar.gov.in/index.aspx"));
        startActivity(intent);
    }
    public void onClick_Button_newRegistration_Button(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://mprojgar.gov.in/CI_JS/frm_UserRegistration.aspx"));
        startActivity(intent);
    }
    public void onClick_Button_RenewOld_Registration(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://mprojgar.gov.in/User_Mgmt/Frm_userlogin_JS.aspx"));
        startActivity(intent);
    }
    public void onClick_Button_FindRegistrationNo(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://mprojgar.gov.in/CI_JS/frm_CI_JS_KnowYourRegNo.aspx"));
        startActivity(intent);
    }
    public void onClick_Button_FindUserIDandPassword(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://mprojgar.gov.in/forgotPassword_JS.aspx"));
        startActivity(intent);
    }
    public void onClick_Button_TransferRegistration_button(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://mprojgar.gov.in/User_Mgmt/Frm_userlogin_JS.aspx"));
        startActivity(intent);
    }
    public void onClick_RatingButton(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mithila_tech.madhyapradeshrojgarsamachar"));
        startActivity(intent);
    }

//    //Test ForceCrash Report
//    public void forceCrash(View view) {
//        throw new RuntimeException("This is a crash");
//    }



//    //---------------------------------------------------------
//    // Action Bar Icon Display Code
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        return super.onCreateOptionsMenu(menu);
//
//    }
//
//    //Action bar menu item code
////    ---------------------------------------------
//    public boolean onOptionsItemSelected(MenuItem item){
//        super.onOptionsItemSelected(item);
//
////        String install = getIntent().getStringExtra("INSTALL");
//
////        if (item.getItemId()== R.id.shopping_Cart){
////            Intent intent = new Intent(Intent.ACTION_VIEW);
////                intent.setData(Uri.parse(install));
//////          choose=Intent.createChooser(intent,"lunchMap");
////                startActivity(intent);
////        }
//
//        switch (item.getItemId()){
//            case R.id.app_Install:
////                Intent intent = new Intent(Intent.ACTION_VIEW);
////                intent.setData(Uri.parse(install));
//////          choose=Intent.createChooser(intent,"lunchMap");
////                startActivity(intent);
////                Log.i("Tulshi", "onCreate: "+install);
//                Intent intent = new Intent(this,App_Store_Activity.class);
//                startActivity(intent);
//                break;
//
//            //Back Action Button Code
//            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//        return true;
//    }
    /** Called when leaving the activity */
    @Override
    public void onPause() {
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TAG"," On Stop");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TAG"," On Restart");
    }
    @Override
    protected void onStart() {
        super.onStart();
        //InterstitialAd AdMob Code
    }
    //-------------------- Save Activity State Code ----------------------------

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        Log.i("TAG","onSaveInstanceState is Coll");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("TAG","onRestoreInstanceState is Coll");
    }
    //-------------------- Save Activity State Code ----------------------------

}
