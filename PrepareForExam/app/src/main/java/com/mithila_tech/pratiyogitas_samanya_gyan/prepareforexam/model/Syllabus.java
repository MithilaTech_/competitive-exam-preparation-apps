package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

/**
 * Created by ASUS on 2/21/2018.
 */

public class Syllabus {
    private String burl;
    private String cdetail;
    private String clanguage;
    private String ctitle;
    //private String syllabus_img_store_date;
    private String syllabus_img_url;

    public Syllabus() {

    }

    public String getBurl() {
        return burl;
    }

    public void setBurl(String burl) {
        this.burl = burl;
    }

    public String getCdetail() {
        return cdetail;
    }

    public void setCdetail(String cdetail) {
        this.cdetail = cdetail;
    }

    public String getClanguage() {
        return clanguage;
    }

    public void setClanguage(String clanguage) {
        this.clanguage = clanguage;
    }

    public String getCtitle() {
        return ctitle;
    }

    public void setCtitle(String ctitle) {
        this.ctitle = ctitle;
    }

/*    public String getSyllabus_img_store_date() {
        return syllabus_img_store_date;
    }

    public void setSyllabus_img_store_date(String syllabus_img_store_date) {
        this.syllabus_img_store_date = syllabus_img_store_date;
    }*/

    public String getSyllabus_img_url() {
        return syllabus_img_url;
    }

    public void setSyllabus_img_url(String syllabus_img_url) {
        this.syllabus_img_url = syllabus_img_url;
    }
}
