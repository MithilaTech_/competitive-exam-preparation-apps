package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.firebase.client.Firebase;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.ConnectivityHelper;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    //firebase auth object
    private static FirebaseAuth firebaseAuth;

    //view objects
    private TextView textViewUserEmail;
    private Button buttonLogout,btnStudy,btnTest;
    private String uname,uemail;
    private static boolean isGoogleSignIn;
    private boolean isFacebookSignin;
    private static GoogleApiClient googleApiClient;
    private CallbackManager callbackManager;
    private Firebase myFirebaseRef;
    private Button btnSubject;
    private Button btnSyllabus;
    private Button btnTopicTestSeries;
    private Button btnSubjectTestSeries;
    private Button btnCommonTestSeries;
    private Button btnCurrentAffairs;
    private Button btnJobs;
    private static Activity activity = null;
    private static Context context;
    private InterstitialAd mInterstitialAd;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView nv;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);//initializing firebase authentication object
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(ProfileActivity.this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        context = this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //Add YOUR Firebase Reference URL instead of the following URL
        Firebase.setAndroidContext(this);
        activity = this;
        myFirebaseRef = new Firebase("https://pratiyogita-samanya-gyan.firebaseio.com/");
        firebaseAuth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        //getting current user
        FirebaseUser user = firebaseAuth.getCurrentUser();
        setUpGoogleSetting();
        //initializing views
        //textViewUserEmail = (TextView) findViewById(R.id.textViewUserEmail);
        nv = (NavigationView) findViewById(R.id.nav_view);
        nv.setNavigationItemSelectedListener(this);
        buttonLogout = (Button) findViewById(R.id.buttonLogout);
        btnStudy = (Button) findViewById(R.id.btnstudy_material);
        btnTest = (Button) findViewById(R.id.btntest);
        btnJobs = (Button) findViewById(R.id.btnjobs);
        btnSyllabus = (Button) findViewById(R.id.btnsyllabus);
        btnTopicTestSeries = (Button) findViewById(R.id.btnTopicTestSeries);
        btnSubjectTestSeries = (Button) findViewById(R.id.btnSubjectTestSeries);
        btnCommonTestSeries = (Button) findViewById(R.id.btnCommonTestSeries);
        btnCurrentAffairs = (Button) findViewById(R.id.btnCurrentAffairs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if(ConnectivityHelper.isRestrictedBackground(this)==true) {
                //goToNotificationSettings(getString(R.string.default_notification_channel_id),this);
                openBackgroundRestrictedAlert();
            }
        }
        if(intent!=null) {
            isGoogleSignIn = intent.getBooleanExtra("isGoogleSignIn",false);
            isFacebookSignin = intent.getBooleanExtra("isfacebbokSignin",false);
            if(isGoogleSignIn || isFacebookSignin) {
                uemail = intent.getStringExtra("uemail");
                uname = intent.getStringExtra("uname");
                /*if(uname!=null || uemail!=null) {
                    textViewUserEmail.setText("Welcome "+uemail);

                }*/

                getSupportActionBar().setTitle(uemail);
                SharedPrefManager.saveEmail(ProfileActivity.this,uemail);
            } else {
                //if the user is not logged in
                //that means current user will return null
                if(firebaseAuth.getCurrentUser() == null){
                    //closing this activity
                    finish();
                    //starting login activity
                    startActivity(new Intent(this, UserLoginActivity.class));
                }
                //displaying logged in user name
                //textViewUserEmail.setText("Welcome "+user.getEmail());

            }
        }

        //adding listener to button
        buttonLogout.setOnClickListener(this);
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    public void openBackgroundRestrictedAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfileActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("Background Restrictted...");

        // Setting Dialog Message
        alertDialog.setMessage("Backround is restricted. Change settings to remove restrictions.");

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.ic_launcher);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                goToNotificationSettings(getString(R.string.default_notification_channel_id),ProfileActivity.this);

            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                Toast.makeText(getApplicationContext(), "Remove background restrictions to view notifications properly.", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void goToNotificationSettings(String channel, Context context) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            intent.addFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
            if (channel != null) {
                intent.setAction(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                intent.putExtra(Settings.EXTRA_CHANNEL_ID, channel);
            } else {
                intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            }
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (channel != null) {
                intent.setAction(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                intent.putExtra(Settings.EXTRA_CHANNEL_ID, channel);
            } else {
                intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            }
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra("app_package", context.getPackageName());
            intent.putExtra("app_uid", context.getApplicationInfo().uid);
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + context.getPackageName()));
        }
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_logout:
                logoutuser(ProfileActivity.this);
                break;

        }
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        //if logout is pressed
        if(view == buttonLogout){
            //logging out the user
            firebaseAuth.signOut();
            startActivity(new Intent(this, WelcomeScreen.class));
            //closing activity
            finish();
            //starting login activity

        }
       if(view == btnStudy){
            Intent i = new Intent(this, SubjectActivity.class);
            i.putExtra("isStudyTest",true);
            i.putExtra("istopictestseries",false);
            i.putExtra("issubjecttestseries",false);
            i.putExtra("iscommontestseries",false);
            startActivity(i);
        }else if(view == btnJobs){
            Intent i = new Intent(this, JobsActivity.class);
            i.putExtra("isStudyTest",false);
            i.putExtra("istopictestseries",false);
            i.putExtra("issubjecttestseries",false);
            i.putExtra("iscommontestseries",false);
            startActivity(i);

        }else if(view == btnCurrentAffairs){
            Intent i = new Intent(this, CurrentAffairsActivity.class);
            i.putExtra("isStudyTest",false);
            i.putExtra("istopictestseries",false);
            i.putExtra("issubjecttestseries",false);
            i.putExtra("iscommontestseries",false);
            startActivity(i);

        }else if(view == btnSyllabus){
            Intent i = new Intent(this, SyllabusActivity.class);
            i.putExtra("isStudyTest",false);
            i.putExtra("istopictestseries",false);
            i.putExtra("issubjecttestseries",false);
            i.putExtra("iscommontestseries",false);
            startActivity(i);
        }
        else if(view == btnTopicTestSeries){
            Intent i = new Intent(this, SubjectActivity.class);
               i.putExtra("isStudyTest",false);
            i.putExtra("istopictestseries",true);
            i.putExtra("issubjecttestseries",false);
            i.putExtra("iscommontestseries",false);
            startActivity(i);
        }else if(view == btnSubjectTestSeries){
            Intent i = new Intent(this, SubjectActivity.class);
            i.putExtra("isStudyTest",false);
            i.putExtra("istopictestseries",false);
            i.putExtra("issubjecttestseries",true);
            i.putExtra("iscommontestseries",false);
            startActivity(i);
        }else if(view == btnCommonTestSeries){
            Intent i = new Intent(this, AvailableTestSeries.class);
            i.putExtra("isStudyTest",false);
            i.putExtra("istopictestseries",false);
            i.putExtra("issubjecttestseries",false);
            i.putExtra("iscommontestseries",true);
            startActivity(i);
        }

    }

    public void setUpGoogleSetting() {
        // Creating and Configuring Google Sign In object.
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();

        // Creating and Configuring Google Api Client.
        googleApiClient = new GoogleApiClient.Builder(ProfileActivity.this)
                .enableAutoManage(ProfileActivity.this , new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
       /* if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }*/


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void logoutuser(Context c) {
        //logging out the user
        firebaseAuth.signOut();
        SharedPrefManager.clear(context);
        activity.finish();
        c.startActivity(new Intent(c, WelcomeScreen.class));


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnulogout:
                logoutuser(ProfileActivity.this);
                break;
            case R.id.mnuPracticeTest:
                Intent i = new Intent(this, SubjectActivity.class);
                i.putExtra("isStudyTest",true);
                i.putExtra("istopictestseries",false);
                i.putExtra("issubjecttestseries",false);
                i.putExtra("iscommontestseries",false);
                startActivity(i);
                break;
            case R.id.mnuTopicTest:i = new Intent(this, SubjectActivity.class);
                i.putExtra("isStudyTest",false);
                i.putExtra("istopictestseries",true);
                i.putExtra("issubjecttestseries",false);
                i.putExtra("iscommontestseries",false);
                startActivity(i);

                break;
            case R.id.mnuSubjectTest:
                i = new Intent(this, SubjectActivity.class);
                i.putExtra("isStudyTest",false);
                i.putExtra("istopictestseries",false);
                i.putExtra("issubjecttestseries",true);
                i.putExtra("iscommontestseries",false);
                startActivity(i);
                break;
            case R.id.mnuCommonTest:
                i = new Intent(this, AvailableTestSeries.class);
                i.putExtra("isStudyTest",false);
                i.putExtra("istopictestseries",false);
                i.putExtra("issubjecttestseries",false);
                i.putExtra("iscommontestseries",true);
                startActivity(i);
                break;
            case R.id.mnuJobs:
                i = new Intent(this, JobsActivity.class);
                i.putExtra("isStudyTest",false);
                i.putExtra("istopictestseries",false);
                i.putExtra("issubjecttestseries",false);
                i.putExtra("iscommontestseries",false);
                startActivity(i);
                break;
            case R.id.mnuSyllabus:
                i = new Intent(this, SyllabusActivity.class);
                i.putExtra("isStudyTest",false);
                i.putExtra("istopictestseries",false);
                i.putExtra("issubjecttestseries",false);
                i.putExtra("iscommontestseries",false);
                startActivity(i);
                break;
            case R.id.mnuSubscribe:
                i = new Intent(this, SubscribeActivity.class);
                i.putExtra("isStudyTest",false);
                i.putExtra("istopictestseries",false);
                i.putExtra("issubjecttestseries",false);
                i.putExtra("iscommontestseries",false);
                startActivity(i);
                break;

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
