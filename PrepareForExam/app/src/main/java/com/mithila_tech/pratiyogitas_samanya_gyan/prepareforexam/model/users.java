package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

/**
 * Created by ASUS on 1/19/2018.
 */

public class users {
    public String name;
    public String email;
    public String mobile;

    public users() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public users(String username, String email,String mobile) {
        this.name = username;
        this.email = email;
        this.mobile = mobile;
    }
}
