package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class RValidation {
    String r_email, r_mobil_no;
    boolean is_mob_number = false, is_email = false;

    //this is the default format for email validation

    String regEx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

// empty constructor for call the object

    public RValidation() {
        super();
    }

// create getter & setter for parameters

    public String getR_email() {
        return r_email;
    }

    public void setR_email(String raj_email) {
        this.r_email = raj_email;
    }

    public String getR_mobil_no() {
        return r_mobil_no;
    }

    public void setR_mobil_no(String raj_mobil_no) {
        this.r_mobil_no = raj_mobil_no;
    }

    //method to validate you mobile number, if it is valid it will return true
    public boolean isIs_mob_number() {
// mobile number should be 10 digit
        Pattern pattern = Pattern.compile("\\d{10}");
        Matcher matchr = pattern.matcher(this.getR_mobil_no().trim());
        if (matchr.matches()) {
            is_mob_number = true;
        }
        return is_mob_number;
    }

    // method to validate you e-mail id, if it is valid it will return true
    public boolean isIs_email() {
        Matcher matcherObj = Pattern.compile(regEx).matcher(
                this.getR_email().trim());
        if (matcherObj.matches()) {
            is_email = true;
        }
        return is_email;
    }

}
