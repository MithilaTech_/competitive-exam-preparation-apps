package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.SubjectHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;

import java.util.List;

/**
 * Created by ASUS on 12/14/2017.
 */

public class TestSubjectAdapter extends RecyclerView.Adapter<SubjectHolder> {
    private List<Subject> task;
    protected Context context;
    public TestSubjectAdapter(Context context, List<Subject> task) {
        this.task = task;
        this.context = context;
    }
    @Override
    public SubjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SubjectHolder viewHolder = null;
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_list, parent, false);
        viewHolder = new SubjectHolder(this.context,layoutView);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(SubjectHolder holder, int position) {
        holder.categoryTitle.setText(task.get(position).getSname());
    }
    @Override
    public int getItemCount() {
        return this.task.size();
    }
}