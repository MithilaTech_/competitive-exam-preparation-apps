package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.QuestionHolder;


import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Question;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.AnimationUtility;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.android.gms.ads.AdView;
import com.shreyaspatil.firebase.recyclerpagination.DatabasePagingOptions;
import com.shreyaspatil.firebase.recyclerpagination.FirebaseRecyclerPagingAdapter;
import com.shreyaspatil.firebase.recyclerpagination.LoadingState;


public class StudyQuestionActivity extends AppCompatActivity {

    private LinearLayout layAnswer;
    private static final String TAG = StudyQuestionActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseRecyclerPagingAdapter recyclerViewAdapter;
    private DatabaseReference databaseReference,quesreference;
    private String tname;
    private ImageLoader imageLoader;
    private List<Question> allQuestions;
    private ProgressDialog mProgressBar;
    private String q_language;
    private int count=0;
    private Query query;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DatabasePagingOptions<Question> options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        if(intent !=null) {
            tname = intent.getStringExtra("topicname");
            if(tname ==null || tname.length()<=0) {
                tname = "भारतीय अर्थव्यवस्था";
            }
        }
        getSupportActionBar().setTitle(tname);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
       // quesreference = databaseReference.child("/questions");
        query = databaseReference.child("/questions").orderByChild("topcname");
        databaseReference.keepSynced(true);

        databaseReference.keepSynced(false);

        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(5)
                .setPageSize(10)
                .build();

        //Initialize FirebasePagingOptions
        options = new DatabasePagingOptions.Builder<Question>()
                .setLifecycleOwner(this)
                .setQuery(query, config, Question.class)

                .build();
        recyclerView = (RecyclerView)findViewById(R.id.questions_list);
        linearLayoutManager = new LinearLayoutManager(this);

        imageLoader = ImageLoader.getInstance();
        q_language = SharedPrefManager.getLang(StudyQuestionActivity.this);

        allQuestions= new ArrayList<Question>();
        mProgressBar = new ProgressDialog(StudyQuestionActivity.this);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setCancelable(false);
        mProgressBar.setMessage(getString(R.string.title_loading));
        mProgressBar.show();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                //Log.e("SubjectActivity","onChildAdded()");
                HashMap<String,String> obj= (HashMap<String, String>) dataSnapshot.getValue();


                    String subname = (String) obj.get("subjectname");
                    String topname = (String) obj.get("topcname");
                    String qlang = (String) obj.get("language");
                    String ques = (String) obj.get("question");
                    String optA = (String) obj.get("Option_A_text");
                    String optAurl = (String) obj.get("Option_A_Image_url");
                    String optB = (String) obj.get("Option_B_text");
                    String optBurl = (String) obj.get("Option_B_Image_url");
                    String optC = (String) obj.get("Option_C_text");
                    String optCurl = (String) obj.get("Option_C_Image_url");
                    String optD = (String) obj.get("Option_D_text");
                    String optDurl = (String) obj.get("Option_D_Image_url");
                    String optE = (String) obj.get("Option_E_text");
                    String optEurl = (String) obj.get("Option_E_Image_url");
                    String c_ans = (String) obj.get("correct_ans");
                    String c_ans_url = (String) obj.get("Correct_Ans_Image_url");
                    String exp = (String) obj.get("explaination");
                    String exp_url = (String) obj.get("Explanation_Image_url");
                    if(topname !=null  && topname.equals(tname)) {
                        Question quest = new Question();
                        quest.setSubjectname(subname);
                        quest.setTopcname(topname);
                        quest.setLanguage(qlang);
                        quest.setQuestion(ques);
                        quest.setOption_A_text(optA);
                        quest.setOption_A_Image_url(optAurl);
                        quest.setOption_B_text(optB);
                        quest.setOption_B_Image_url(optBurl);
                        quest.setOption_C_text(optC);
                        quest.setOption_C_Image_url(optCurl);
                        quest.setOption_D_text(optD);
                        quest.setOption_D_Image_url(optDurl);
                        quest.setOption_E_text(optE);
                        quest.setOption_E_Image_url(optEurl);
                        quest.setCorrect_ans(c_ans);
                        quest.setCorrect_Ans_Image_url(c_ans_url);
                        quest.setExplaination(exp);
                        quest.setExplanation_Image_url(exp_url);
                        allQuestions.add(quest);
                    }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(allQuestions!=null && allQuestions.size()>0) {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }

                        fillData();


                } else {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }
                    Toast.makeText(StudyQuestionActivity.this, "No questions found...", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


    public void toggle_contents(View view) {

    }
    String ansval = "A";
    public void fillData() {
        recyclerViewAdapter = new FirebaseRecyclerPagingAdapter<Question,QuestionHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final QuestionHolder questionHolder, int i, @NonNull Question question) {
                if (i < allQuestions.size()) {
                    //
                    final Question q = allQuestions.get(i);
                    //if (q.getTopcname().equals(tname)) {
                    questionHolder.questioncount.setText(String.valueOf(i+1)+".");
                    questionHolder.question.setText(q.getQuestion());
                    //  Log.e("StudyQuestionActivity",q.getQuestion());
                    if (q.getQuestions_Image_url()!=null && !q.getQuestions_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.qImg.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getQuestions_Image_url(), questionHolder.qImg);
                    }
                    questionHolder.optA.setText(q.getOption_A_text());
                    if (q.getOption_A_Image_url()!=null && !q.getOption_A_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.optAImg.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getOption_A_Image_url(), questionHolder.optAImg);
                    }
                    questionHolder.optB.setText(q.getOption_B_text());
                    if (q.getOption_B_Image_url()!=null && !q.getOption_B_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.optBImg.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getOption_B_Image_url(), questionHolder.optBImg);
                    }
                    questionHolder.optC.setText(q.getOption_C_text());
                    if (q.getOption_C_Image_url()!=null && !q.getOption_C_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.optCImg.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getOption_C_Image_url(), questionHolder.optCImg);
                    }
                    questionHolder.optD.setText(q.getOption_D_text());
                    if (!q.getOption_D_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.optDImg.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getOption_D_Image_url(), questionHolder.optDImg);
                    }
                    questionHolder.optE.setText(q.getOption_E_text());
                    if (q.getOption_E_Image_url()!=null && !q.getOption_E_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.optEImg.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getOption_E_Image_url(), questionHolder.optEImg);
                    }

                    questionHolder.correct_ans.setText(q.getCorrect_ans());
                    if (q.getCorrect_Ans_Image_url()!=null && !q.getCorrect_Ans_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.correct_ans_img.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getCorrect_Ans_Image_url(), questionHolder.correct_ans_img);
                    }

                    questionHolder.explain.setText(q.getExplaination());
                    if (q.getExplanation_Image_url() !=null && !q.getExplanation_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                        questionHolder.exp_img.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(q.getExplanation_Image_url(), questionHolder.exp_img);
                    }

                    questionHolder.optA.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ansval ="A";
                            if(ansval.equals(q.getCorrect_ans())) {
                                view.setBackgroundResource(R.drawable.shadow_right_button);
                            } else {
                                view.setBackgroundResource(R.drawable.shadow_wrong_button);
                            }
                            setAnswerStates(q,questionHolder);
                        }
                    });
                    questionHolder.optB.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ansval ="B";
                            if(ansval.equals(q.getCorrect_ans())) {
                                view.setBackgroundResource(R.drawable.shadow_right_button);

                            }else {
                                view.setBackgroundResource(R.drawable.shadow_wrong_button);
                            }
                            setAnswerStates(q,questionHolder);
                        }
                    });

                    questionHolder.optC.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ansval ="C";
                            if(ansval.equals(q.getCorrect_ans())) {
                                view.setBackgroundResource(R.drawable.shadow_right_button);

                            }else {
                                view.setBackgroundResource(R.drawable.shadow_wrong_button);
                            }
                            setAnswerStates(q,questionHolder);
                        }
                    });

                    questionHolder.optD.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ansval ="D";
                            if(ansval.equals(q.getCorrect_ans())) {
                                view.setBackgroundResource(R.drawable.shadow_right_button);

                            }else {
                                view.setBackgroundResource(R.drawable.shadow_wrong_button);
                            }
                            setAnswerStates(q,questionHolder);
                        }
                    });

                    questionHolder.optE.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ansval ="E";
                            if(ansval.equals(q.getCorrect_ans())) {
                                view.setBackgroundResource(R.drawable.shadow_right_button);

                            }else {
                                view.setBackgroundResource(R.drawable.shadow_wrong_button);
                            }
                            setAnswerStates(q,questionHolder);
                        }
                    });

                    questionHolder.txtAnswer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Log.e("StudyQuestion","OnAnsClick");
                            if (questionHolder.layoutAns.isShown()) {
                                AnimationUtility.slide_up(StudyQuestionActivity.this, questionHolder.layoutAns);
                                questionHolder.layoutAns.setVisibility(View.GONE);
                            } else {
                                questionHolder.layoutAns.setVisibility(View.VISIBLE);
                                AnimationUtility.slide_down(StudyQuestionActivity.this, questionHolder.layoutAns);
                            }
                        }
                    });
                    //}
                }
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        // Do your loading animation
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        // Stop Animation
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        //Reached end of Data set
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        retry();
                        break;
                }
            }

            @Override
            protected void onError(@NonNull DatabaseError databaseError) {
                super.onError(databaseError);
                mSwipeRefreshLayout.setRefreshing(false);
                databaseError.toException().printStackTrace();
            }

            @Override
            public QuestionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.question_list, parent, false);

                return new QuestionHolder(StudyQuestionActivity.this,view);

            }
            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public int getItemViewType(int position) {
                return position;
            }

           /* @Override
            protected void populateViewHolder
                    (final QuestionHolder holder, final Question journalEntry, int position) {

            }*/
        };

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);
        //Set listener to SwipeRefreshLayout for refresh action
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerViewAdapter.refresh();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void setAnswerStates(Question q,QuestionHolder holder ) {
      if(q.getCorrect_ans().equals("A")) {
          holder.optA.setBackgroundResource(R.drawable.shadow_right_button);
          holder.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
      } else  if(q.getCorrect_ans().equals("B")) {
          holder.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optB.setBackgroundResource(R.drawable.shadow_right_button);
          holder.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
      }else  if(q.getCorrect_ans().equals("C")) {
          holder.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optC.setBackgroundResource(R.drawable.shadow_right_button);
          holder.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
      }else  if(q.getCorrect_ans().equals("D")) {
          holder.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optD.setBackgroundResource(R.drawable.shadow_right_button);
          holder.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
      }else  if(q.getCorrect_ans().equals("E")) {
          holder.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
          holder.optE.setBackgroundResource(R.drawable.shadow_right_button);
      }


    }

    public void onClick(View view) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.stopListening();
        }
    }

}
