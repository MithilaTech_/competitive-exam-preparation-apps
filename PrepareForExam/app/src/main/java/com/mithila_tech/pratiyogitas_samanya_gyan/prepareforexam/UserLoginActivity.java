package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.ConnectivityHelper;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.RValidation;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;

public class UserLoginActivity extends AppCompatActivity  implements View.OnClickListener {
    //defining views
    private ImageView buttonSignIn,buttonSignUp;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewSignup,textViewReset,textViewPhone;

    //firebase auth object
    private FirebaseAuth firebaseAuth;

    //progress dialog
    private ProgressDialog progressDialog;
    private InterstitialAd mInterstitialAd;
    private RValidation r_valid;
    private boolean sucess_regist=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("User Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getting firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();
        r_valid=new RValidation();
        //if the objects getcurrentuser method is not null
        //means user is already logged in
        if(firebaseAuth.getCurrentUser() != null){
            //close this activity
            finish();
            //opening profile activity
            startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
        }

        //initializing views
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonSignIn = (ImageView) findViewById(R.id.buttonSignin);
        buttonSignUp  = (ImageView) findViewById(R.id.buttonSignup);
        textViewReset = (TextView)findViewById(R.id.textViewReset) ;
        textViewPhone = (TextView)findViewById(R.id.textViewPhone) ;

        progressDialog = new ProgressDialog(this);

        //attaching click listener
        buttonSignIn.setOnClickListener(this);
       buttonSignUp.setOnClickListener(this);
        textViewReset.setOnClickListener(this);
        textViewPhone.setOnClickListener(this);
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }

    //method for user login
    private void userLogin(){
        if(ConnectivityHelper.isConnectedToNetwork(UserLoginActivity.this)) {
            final String email = editTextEmail.getText().toString().trim();
            String password = editTextPassword.getText().toString().trim();


            //checking if email and passwords are empty
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Please enter email", Toast.LENGTH_LONG).show();
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
                return;
            }

            r_valid.setR_email(email.trim());
            if (!r_valid.isIs_email()) {
                Toast.makeText(this, "Please enter valid email", Toast.LENGTH_LONG).show();
                return;
            }
            //if the email and password are not empty
            //displaying a progress dialog

            progressDialog.setMessage("Logging Please Wait...");
            progressDialog.show();

            //logging in the user
            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            progressDialog.dismiss();
                            //if the task is successfull
                            if (task.isSuccessful()) {
                                //start the profile activity
                                SharedPrefManager.saveEmail(UserLoginActivity.this, email);
                                finish();
                                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                            } else {
                                Toast.makeText(UserLoginActivity.this, "Username or Password is invalid", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(UserLoginActivity.this, "Please check internet connection..", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view) {
        if(view == buttonSignIn){
            userLogin();
        }

        if(view == buttonSignUp){
            finish();
            startActivity(new Intent(this, UserSignUpActivity.class));
        }

        if(view == textViewReset){
            finish();
            startActivity(new Intent(this, ResetPasswordActivity.class));
        }
        if(view == textViewPhone){
            finish();
            startActivity(new Intent(this, PhoneLoginActivity.class));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
