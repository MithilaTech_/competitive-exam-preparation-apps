package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters.TopicTestIdAdapter;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.TopicTestId;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.DialogClickInterface;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.HashMap;

public class AvailableTestSeries extends AppCompatActivity implements DialogClickInterface {

    private GridView test_id_grid;
    private String topc_language;
    private String tname,subname,topicname,commontestname;
    private ProgressDialog mProgressBar;
    private DatabaseReference databaseReference,topicreference;
    private Query query;
    private ArrayList<String> allTest;
    private TopicTestIdAdapter testIdsAdapter;
    private boolean istopictestseries,issubjecttestseries,iscommontestseries;
    private int identifier = 0;
    private int mPosition =0;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_test_series);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();

        topc_language = SharedPrefManager.getLang(AvailableTestSeries.this);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.keepSynced(true);

        databaseReference.keepSynced(false);
        topicreference = databaseReference.child("/test_series");
        allTest= new ArrayList<String>();
        test_id_grid = (GridView) findViewById(R.id.test_series_grid) ;
        mProgressBar = new ProgressDialog(AvailableTestSeries.this);

        if(intent !=null) {

            istopictestseries = intent.getBooleanExtra("istopictestseries",false);
            issubjecttestseries = intent.getBooleanExtra("issubjecttestseries",false);
            iscommontestseries = intent.getBooleanExtra("iscommontestseries",false);
            if(istopictestseries) {
                subname = "NA";
                commontestname="NA";
                topicname = intent.getStringExtra("topicname");
                tname = topicname;
                query =  topicreference.orderByChild("topcname").equalTo(tname);
            } else if(issubjecttestseries) {
                subname = intent.getStringExtra("subjectname");
                topicname ="NA";
                commontestname ="NA";
                tname = subname;
                query =  topicreference.orderByChild("subjectname").equalTo(tname);
            } else {
                subname = "NA";
                topicname ="NA";
                commontestname ="common";

                tname =commontestname;
                query =  topicreference.orderByChild("common_test_id");
            }

        }
        getSupportActionBar().setTitle(tname);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressBar.setIndeterminate(true);
        mProgressBar.setMessage(getString(R.string.title_loading));
        mProgressBar.show();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                HashMap<String,String> ss= (HashMap<String, String>) dataSnapshot.getValue();
                String testid="";
                String lang=ss.get("language");
                if(lang.equals(topc_language)) {
                    if (istopictestseries) {
                        testid = (String) ss.get("topic_test_id");
                    } else if (issubjecttestseries) {
                        testid = (String) ss.get("sub_test_id");
                    } else if (iscommontestseries) {
                        testid = (String) ss.get("common_test_id");
                    }

                    TopicTestId topic = new TopicTestId();
                    topic.setTopic_test_id(testid);
                    if (!allTest.contains(testid)) {
                        allTest.add(testid);
                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(allTest!=null && allTest.size()>0) {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }


                    testIdsAdapter = new TopicTestIdAdapter(AvailableTestSeries.this,allTest);
                    test_id_grid.setAdapter(testIdsAdapter);
                    test_id_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            mPosition = position;
                            CustomAlertDialog.getInstance().showConfirmDialog("Instructions", "Rules", "Ok", "Cancel", AvailableTestSeries.this, identifier);
                        }
                    });

                } else {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }
                    Toast.makeText(AvailableTestSeries.this, "No test series found...", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClickPositiveButton(DialogInterface pDialog, int pDialogIntefier) {
        Intent i = new Intent(AvailableTestSeries.this, TestSeriesQuestionsActivity.class);
        i.putExtra("topicname", topicname);
        i.putExtra("subjectname", subname);
        i.putExtra("commontestname", commontestname);
        i.putExtra("testtitle", tname);
        i.putExtra("topicid", allTest.get(mPosition));
        i.putExtra("istopictestseries",istopictestseries);
        i.putExtra("issubjecttestseries",issubjecttestseries);
        i.putExtra("iscommontestseries",iscommontestseries);
         CustomAlertDialog.getInstance().closeDialog();
        AvailableTestSeries.this.startActivity(i);
    }

    @Override
    public void onClickNegativeButton(DialogInterface pDialog, int pDialogIntefier) {
       CustomAlertDialog.getInstance().closeDialog();
        finish();
    }
}
