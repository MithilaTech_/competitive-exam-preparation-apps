package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

/**
 * Created by ASUS on 3/29/2018.
 */

public class scores {
    private int score;
    private String test_detail;
    private String uid;
    private String uname;
    private String uname_detail;



    public scores() {

    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTest_detail() {
        return test_detail;
    }

    public void setTest_detail(String test_detail) {
        this.test_detail = test_detail;
    }

    public String getUname_detail() {
        return uname_detail;
    }

    public void setUname_detail(String uname_detail) {
        this.uname_detail = uname_detail;
    }
}
