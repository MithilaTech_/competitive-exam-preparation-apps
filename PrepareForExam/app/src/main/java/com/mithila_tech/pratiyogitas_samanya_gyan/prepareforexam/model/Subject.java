package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

/**
 * Created by ASUS on 12/14/2017.
 */

public class Subject {
    private String slanguage;
    private String sname;
    private String sid;

    public Subject() {

    }

    public String getSlanguage() {
        return slanguage;
    }

    public void setSlanguage(String slanguage) {
        this.slanguage = slanguage;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
