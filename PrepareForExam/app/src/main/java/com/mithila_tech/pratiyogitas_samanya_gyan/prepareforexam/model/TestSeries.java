package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ASUS on 12/15/2017.
 */

public class TestSeries implements Parcelable {
    private String common_test_id;
    private String Correct_Ans_Image_url;
    private String Correct_Ans_Text;
    private String Explaination_Text;
    private String Explanation_Image_url;
    private String language;
    private String Option_A_Image_url;
    private String Option_A_text;
    private String Option_B_Image_url;
    private String Option_B_text;
    private String Option_C_Image_url;
    private String Option_C_text;
    private String Option_D_Image_url;
    private String Option_D_text;
    private String Option_E_Image_url;
    private String Option_E_text;
    private String question;
    private String Questions_Image_url;
    private String sub_test_id;
    private String subjectname;
    private String timeinminutes;
    private String topcname;
    private String topic_test_id;

    public TestSeries() {

    }

    public TestSeries(Parcel source) {
        common_test_id = source.readString();
        Correct_Ans_Image_url = source.readString();
        Correct_Ans_Text = source.readString();
        Explaination_Text = source.readString();
        Explanation_Image_url = source.readString();
        language = source.readString();
        Option_A_Image_url = source.readString();
        Option_A_text = source.readString();
        Option_B_Image_url = source.readString();
        Option_B_text = source.readString();
        Option_C_Image_url = source.readString();
        Option_C_text = source.readString();
        Option_D_Image_url = source.readString();
        Option_D_text = source.readString();
        Option_E_Image_url = source.readString();
        Option_E_text = source.readString();
        question = source.readString();
        Questions_Image_url = source.readString();
        sub_test_id = source.readString();
        subjectname = source.readString();
        timeinminutes = source.readString();
        topcname = source.readString();
        topic_test_id = source.readString();
    }

    public String getSubjectname() {
        return subjectname;
    }

    public void setSubjectname(String subjectname) {
        this.subjectname = subjectname;
    }

    public String getTopcname() {
        return topcname;
    }

    public void setTopcname(String topcname) {
        this.topcname = topcname;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestions_Image_url() {
        return Questions_Image_url;
    }

    public void setQuestions_Image_url(String questions_Image_url) {
        Questions_Image_url = questions_Image_url;
    }

    public String getOption_A_text() {
        return Option_A_text;
    }

    public void setOption_A_text(String option_A_text) {
        Option_A_text = option_A_text;
    }

    public String getOption_A_Image_url() {
        return Option_A_Image_url;
    }

    public void setOption_A_Image_url(String option_A_Image_url) {
        Option_A_Image_url = option_A_Image_url;
    }

    public String getOption_B_text() {
        return Option_B_text;
    }

    public void setOption_B_text(String option_B_text) {
        Option_B_text = option_B_text;
    }

    public String getOption_B_Image_url() {
        return Option_B_Image_url;
    }

    public void setOption_B_Image_url(String option_B_Image_url) {
        Option_B_Image_url = option_B_Image_url;
    }

    public String getOption_C_text() {
        return Option_C_text;
    }

    public void setOption_C_text(String option_C_text) {
        Option_C_text = option_C_text;
    }

    public String getOption_C_Image_url() {
        return Option_C_Image_url;
    }

    public void setOption_C_Image_url(String option_C_Image_url) {
        Option_C_Image_url = option_C_Image_url;
    }

    public String getOption_D_text() {
        return Option_D_text;
    }

    public void setOption_D_text(String option_D_text) {
        Option_D_text = option_D_text;
    }

    public String getOption_D_Image_url() {
        return Option_D_Image_url;
    }

    public void setOption_D_Image_url(String option_D_Image_url) {
        Option_D_Image_url = option_D_Image_url;
    }

    public String getOption_E_text() {
        return Option_E_text;
    }

    public void setOption_E_text(String option_E_text) {
        Option_E_text = option_E_text;
    }

    public String getOption_E_Image_url() {
        return Option_E_Image_url;
    }

    public void setOption_E_Image_url(String option_E_Image_url) {
        Option_E_Image_url = option_E_Image_url;
    }

    public String getCorrect_ans() {
        return Correct_Ans_Text;
    }

    public void setCorrect_ans(String correct_ans) {
        this.Correct_Ans_Text = correct_ans;
    }

    public String getCorrect_Ans_Image_url() {
        return Correct_Ans_Image_url;
    }

    public void setCorrect_Ans_Image_url(String correct_Ans_Image_url) {
        Correct_Ans_Image_url = correct_Ans_Image_url;
    }

    public String getExplaination() {
        return Explaination_Text;
    }

    public void setExplaination(String explaination) {
        this.Explaination_Text = explaination;
    }

    public String getExplanation_Image_url() {
        return Explanation_Image_url;
    }

    public void setExplanation_Image_url(String explanation_Image_url) {
        Explanation_Image_url = explanation_Image_url;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(common_test_id);
        dest.writeString(Correct_Ans_Text);
        dest.writeString(Correct_Ans_Image_url);
        dest.writeString(Explaination_Text);
        dest.writeString(Explanation_Image_url);
        dest.writeString(language);
        dest.writeString(Option_A_Image_url);
        dest.writeString(Option_A_text);
        dest.writeString(Option_B_Image_url);
        dest.writeString(Option_B_text);
        dest.writeString(Option_C_Image_url);
        dest.writeString(Option_C_text);
        dest.writeString(Option_D_Image_url);
        dest.writeString(Option_D_text);
        dest.writeString(Option_E_Image_url);
        dest.writeString(Option_E_text);
        dest.writeString(question);
        dest.writeString(Questions_Image_url);
        dest.writeString(sub_test_id);
        dest.writeString(subjectname);
        dest.writeString(timeinminutes);
        dest.writeString(topcname);
        dest.writeString(topic_test_id);
    }


    public static final Creator CREATOR
            = new Creator() {
        public TestSeries createFromParcel(Parcel in) {
            return new TestSeries(in);
        }

        public TestSeries[] newArray(int size) {
            return new TestSeries[size];
        }
    };

    public String getCommon_test_id() {
        return common_test_id;
    }

    public void setCommon_test_id(String common_test_id) {
        this.common_test_id = common_test_id;
    }

    public String getSub_test_id() {
        return sub_test_id;
    }

    public void setSub_test_id(String sub_test_id) {
        this.sub_test_id = sub_test_id;
    }

    public String getTimeinminutes() {
        return timeinminutes;
    }

    public void setTimeinminutes(String timeinminutes) {
        this.timeinminutes = timeinminutes;
    }

    public String getTopic_test_id() {
        return topic_test_id;
    }

    public void setTopic_test_id(String topic_test_id) {
        this.topic_test_id = topic_test_id;
    }
}
