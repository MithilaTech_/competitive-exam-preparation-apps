package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

/**
 * Created by ASUS on 12/14/2017.
 */

public class Topic  {
        private String tlanguage;
        private String tname;
        private String sname;
        private String tid;


    public Topic(){}
    public String getTlanguage() {
        return tlanguage;
    }

    public void setTlanguage(String tlanguage) {
        this.tlanguage = tlanguage;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }
}