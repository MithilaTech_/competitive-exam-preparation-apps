package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ASUS on 12/15/2017.
 */

public class Question implements Parcelable {
    private String subjectname;
    private String topcname;
    private String language;
    private String question;
    private String Questions_Image_url;
    private String Option_A_text;
    private String Option_A_Image_url;
    private String Option_B_text;
    private String Option_B_Image_url;
    private String Option_C_text;
    private String Option_C_Image_url;
    private String Option_D_text;
    private String Option_D_Image_url;
    private String Option_E_text;
    private String Option_E_Image_url;
    private String correct_ans;
    private String Correct_Ans_Image_url;
    private String explaination;
    private String Explanation_Image_url;

    public Question() {

    }

    public Question(Parcel source) {
        subjectname = source.readString();
        topcname = source.readString();
        language = source.readString();
        question = source.readString();
        Questions_Image_url = source.readString();
        Option_A_text = source.readString();
        Option_A_Image_url = source.readString();
        Option_B_text = source.readString();
        Option_B_Image_url = source.readString();
        Option_C_text = source.readString();
        Option_C_Image_url = source.readString();
        Option_D_text = source.readString();
        Option_D_Image_url = source.readString();
        Option_E_text = source.readString();
        Option_E_Image_url = source.readString();
        correct_ans = source.readString();
        Correct_Ans_Image_url = source.readString();
        explaination = source.readString();
        Explanation_Image_url = source.readString();
    }
    public String getSubjectname() {
        return subjectname;
    }

    public void setSubjectname(String subjectname) {
        this.subjectname = subjectname;
    }

    public String getTopcname() {
        return topcname;
    }

    public void setTopcname(String topcname) {
        this.topcname = topcname;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestions_Image_url() {
        return Questions_Image_url;
    }

    public void setQuestions_Image_url(String questions_Image_url) {
        Questions_Image_url = questions_Image_url;
    }

    public String getOption_A_text() {
        return Option_A_text;
    }

    public void setOption_A_text(String option_A_text) {
        Option_A_text = option_A_text;
    }

    public String getOption_A_Image_url() {
        return Option_A_Image_url;
    }

    public void setOption_A_Image_url(String option_A_Image_url) {
        Option_A_Image_url = option_A_Image_url;
    }

    public String getOption_B_text() {
        return Option_B_text;
    }

    public void setOption_B_text(String option_B_text) {
        Option_B_text = option_B_text;
    }

    public String getOption_B_Image_url() {
        return Option_B_Image_url;
    }

    public void setOption_B_Image_url(String option_B_Image_url) {
        Option_B_Image_url = option_B_Image_url;
    }

    public String getOption_C_text() {
        return Option_C_text;
    }

    public void setOption_C_text(String option_C_text) {
        Option_C_text = option_C_text;
    }

    public String getOption_C_Image_url() {
        return Option_C_Image_url;
    }

    public void setOption_C_Image_url(String option_C_Image_url) {
        Option_C_Image_url = option_C_Image_url;
    }

    public String getOption_D_text() {
        return Option_D_text;
    }

    public void setOption_D_text(String option_D_text) {
        Option_D_text = option_D_text;
    }

    public String getOption_D_Image_url() {
        return Option_D_Image_url;
    }

    public void setOption_D_Image_url(String option_D_Image_url) {
        Option_D_Image_url = option_D_Image_url;
    }

    public String getOption_E_text() {
        return Option_E_text;
    }

    public void setOption_E_text(String option_E_text) {
        Option_E_text = option_E_text;
    }

    public String getOption_E_Image_url() {
        return Option_E_Image_url;
    }

    public void setOption_E_Image_url(String option_E_Image_url) {
        Option_E_Image_url = option_E_Image_url;
    }

    public String getCorrect_ans() {
        return correct_ans;
    }

    public void setCorrect_ans(String correct_ans) {
        this.correct_ans = correct_ans;
    }

    public String getCorrect_Ans_Image_url() {
        return Correct_Ans_Image_url;
    }

    public void setCorrect_Ans_Image_url(String correct_Ans_Image_url) {
        Correct_Ans_Image_url = correct_Ans_Image_url;
    }

    public String getExplaination() {
        return explaination;
    }

    public void setExplaination(String explaination) {
        this.explaination = explaination;
    }

    public String getExplanation_Image_url() {
        return Explanation_Image_url;
    }

    public void setExplanation_Image_url(String explanation_Image_url) {
        Explanation_Image_url = explanation_Image_url;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subjectname);
        dest.writeString(topcname);
        dest.writeString(language);
        dest.writeString(question);
        dest.writeString(Questions_Image_url);
        dest.writeString(Option_A_text);
        dest.writeString(Option_A_Image_url);
        dest.writeString(Option_B_text);
        dest.writeString(Option_B_Image_url);
        dest.writeString(Option_C_text);
        dest.writeString(Option_C_Image_url);
        dest.writeString(Option_D_text);
        dest.writeString(Option_D_Image_url);
        dest.writeString(Option_E_text);
        dest.writeString(Option_E_Image_url);
        dest.writeString(correct_ans);
        dest.writeString(Correct_Ans_Image_url);
        dest.writeString(explaination);
        dest.writeString(Explanation_Image_url);


    }


    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}
