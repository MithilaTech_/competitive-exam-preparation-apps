package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;

/**
 * Created by ASUS on 12/17/2017.
 */

public class AnimationUtility {
    /**
 *
 * @param ctx
 * @param v
 */
public static void slide_down(Context ctx, View v) {

    Animation a = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
    if (a != null) {
        a.reset();
        if (v != null) {
            v.clearAnimation();
            v.startAnimation(a);
        }
    }
}

    /**
     *
     * @param ctx
     * @param v
     */
    public static void slide_up(Context ctx, View v) {

        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.slide_up);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }


}
