package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.TopicTestId;

import java.util.ArrayList;

/**
 * Created by ASUS on 3/20/2018.
 */

public class TopicTestIdAdapter extends BaseAdapter {
    private Context mContext;
    private final ArrayList<String> testIds;

    public TopicTestIdAdapter(Context c,ArrayList<String> testIds ) {
        mContext = c;
        this.testIds = testIds;

    }

    @Override
    public int getCount() {
        return testIds.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.topic_test_series_single, null);
            TextView textView = (TextView) grid.findViewById(R.id.txtSeriesNo);

            textView.setText("Test Series "+testIds.get(position));

        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}
