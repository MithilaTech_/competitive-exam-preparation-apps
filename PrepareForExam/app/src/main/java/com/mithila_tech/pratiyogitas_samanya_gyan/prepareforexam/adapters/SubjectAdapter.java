package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.AvailableTestSeries;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.TopicActivity;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.BaseViewHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;

import java.util.List;

/*import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.UnifiedNativeAdViewHolder;*/

/**
 * Created by ASUS on 12/14/2017.
 */

public class SubjectAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<Object> task;
    protected Context context;
    int counter=0;
    private final int ITEM_VIEW_TYPE =0;
    private final int UNIFIED_NATIVE_AD_TYPE =1;
    private static final int VIEW_TYPE_LOADING = 2;
    private boolean isLoaderVisible = false;
    private boolean flag,istopictestseries,issubjecttestseries,iscommontestseries;

    public SubjectAdapter(Context context, List<Object> task,boolean flag,boolean istopictestseries,boolean issubjecttestseries,boolean iscommontestseries) {
        this.task = task;
        this.context = context;
        this.flag = flag;
        this.istopictestseries = istopictestseries;
        this.issubjecttestseries = issubjecttestseries;
        this.iscommontestseries = iscommontestseries;
    }
    @Override
        public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch(viewType) {
            case UNIFIED_NATIVE_AD_TYPE:
                View unifiedNativeLayoutView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.ad_unified, parent, false);

                return new UnifiedNativeAdViewHolder(unifiedNativeLayoutView);
            case VIEW_TYPE_LOADING:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));

            case ITEM_VIEW_TYPE:
            default:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.subject_list, parent, false);

                return new SubjectHolder(context, view);

        }
    }
    @Override
    public void onBindViewHolder(final BaseViewHolder holder, int position) {
        holder.onBind(position);


    }
    @Override
    public int getItemCount() {
        return this.task.size();
    }
    @Override
    public int getItemViewType(int position) {
         Log.e("SubjectActivity","Loader:"+isLoaderVisible);
        if (isLoaderVisible) {
            // return position == task.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
            if (position == task.size()) {
                return VIEW_TYPE_LOADING;

            } else {
                if (task.get(position) instanceof Subject)
                    return ITEM_VIEW_TYPE;
                else
                    return UNIFIED_NATIVE_AD_TYPE;
            }
        } else {
            if (task.get(position) instanceof Subject)
                return ITEM_VIEW_TYPE;
            else
                return UNIFIED_NATIVE_AD_TYPE;
        }
    }

        public void addItems(List<Object> postItems) {
            task.addAll(postItems);
            notifyDataSetChanged();
        }

        public void addLoading() {
            isLoaderVisible = true;
            task.add(new Subject());
            notifyItemInserted(task.size() - 1);
        }

        public void removeLoading() {
            isLoaderVisible = false;
            int position = task.size() - 1;
            Object item = getItem(position);
            if (item != null) {
                task.remove(position);
                notifyItemRemoved(position);
            }
        }
    Object getItem(int position) {
        return task.get(position);
    }

        public void clear() {
            task.clear();
            notifyDataSetChanged();
        }






    public class ProgressHolder extends BaseViewHolder {
        ProgressHolder(View itemView) {
            super(itemView);

        }

        @Override
        protected void clear() {
        }
    }

    public class SubjectHolder extends BaseViewHolder{
        private final String TAG = com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.SubjectHolder.class.getSimpleName();

        public TextView categoryTitle;


        public SubjectHolder(final Context context, final View itemView) {
            super(itemView);

            categoryTitle = (TextView)itemView.findViewById(R.id.subject_title);


        }
        public void setItem(Subject post){
            categoryTitle.setText(post.getSname());

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            if (position < task.size()) {
                // Log.e("SubjectActivity","count:"+position);
                final Subject subject = (Subject) task.get(position);
                if (subject != null) {
                    String firstLetter = subject.getSname();
                    Log.e("SubjectActivity","Viewtype:"+firstLetter);
                    if (firstLetter != null && firstLetter.length() > 0) {

                        setItem(subject);
                    }

                }
               categoryTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(categoryTitle.getText()) && (flag || istopictestseries)) {
                            String taskTitle = subject.getSname();
                            Intent i = new Intent(context, TopicActivity.class);
                            i.putExtra("subjectname", taskTitle);
                            i.putExtra("isStudyTest", flag);
                            i.putExtra("istopictestseries", istopictestseries);
                            i.putExtra("issubjecttestseries", issubjecttestseries);
                            i.putExtra("iscommontestseries", iscommontestseries);
                            context.startActivity(i);
                        } else if (!TextUtils.isEmpty(categoryTitle.getText()) && (issubjecttestseries)) {
                            String taskTitle = subject.getSname();
                            Intent i = new Intent(context, AvailableTestSeries.class);
                            i.putExtra("subjectname", taskTitle);
                            i.putExtra("istopictestseries", istopictestseries);
                            i.putExtra("issubjecttestseries", issubjecttestseries);
                            i.putExtra("iscommontestseries", iscommontestseries);
                            context.startActivity(i);
                        } else {
                            Toast.makeText(context, "No topics found...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }


        }

        @Override
        protected void clear() {

        }
    }

    public class UnifiedNativeAdViewHolder extends BaseViewHolder {
        private UnifiedNativeAdView adView;
        public UnifiedNativeAdViewHolder(@NonNull View itemView) {
            super(itemView);
            adView = itemView.findViewById(R.id.ad_view);
            adView.setHeadlineView(itemView.findViewById(R.id.ad_headline));
            adView.setBodyView(itemView.findViewById(R.id.ad_body));
            adView.setCallToActionView(itemView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(itemView.findViewById(R.id.ad_icon));
            adView.setPriceView(itemView.findViewById(R.id.ad_price));
            adView.setStarRatingView(itemView.findViewById(R.id.ad_stars));
            adView.setStoreView(itemView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(itemView.findViewById(R.id.ad_advertiser));


        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);

            UnifiedNativeAd unifiedNativeAd = (UnifiedNativeAd) task.get(position);
            populateNativeAdView(unifiedNativeAd,adView);
        }
        private void populateNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView adView) {
            ((TextView)adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
            ((TextView)adView.getBodyView()).setText(unifiedNativeAd.getBody());
            ((TextView)adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());

            NativeAd.Image icon = unifiedNativeAd.getIcon();
            if(icon==null){
                adView.getIconView().setVisibility(View.INVISIBLE);
            } else {
                ((ImageView)adView.getIconView()).setImageDrawable(icon.getDrawable());
                adView.getIconView().setVisibility(View.VISIBLE);
            }

            if(unifiedNativeAd.getPrice()==null) {
                adView.getPriceView().setVisibility(View.INVISIBLE);
            } else {
                adView.getPriceView().setVisibility(View.VISIBLE);
                ((TextView)adView.getPriceView()).setText(unifiedNativeAd.getPrice());
            }

            if(unifiedNativeAd.getStore()==null) {
                adView.getStoreView().setVisibility(View.INVISIBLE);
            } else {
                adView.getStoreView().setVisibility(View.VISIBLE);
                ((TextView)adView.getStoreView()).setText(unifiedNativeAd.getStore());
            }

            if(unifiedNativeAd.getStarRating()==null) {
                adView.getStarRatingView().setVisibility(View.INVISIBLE);
            } else {
                adView.getStarRatingView().setVisibility(View.VISIBLE);
                ((RatingBar)adView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
            }

            if(unifiedNativeAd.getAdvertiser()==null) {
                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
            } else {
                adView.getAdvertiserView().setVisibility(View.VISIBLE);
                ((TextView)adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            }

            adView.setNativeAd(unifiedNativeAd);
        }

        public UnifiedNativeAdView getAdView(){
            return  adView;
        }
    }

}