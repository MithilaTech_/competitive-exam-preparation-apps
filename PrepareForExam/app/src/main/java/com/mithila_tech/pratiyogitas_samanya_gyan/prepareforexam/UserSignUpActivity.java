package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.users;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.ConnectivityHelper;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserSignUpActivity extends AppCompatActivity  implements View.OnClickListener {

    //defining view objects
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPhone;


    private ImageView btnSignin;
    private ImageView btnLogin;

    private ProgressDialog progressDialog;


    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_up);
        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

        //if getCurrentUser does not returns null
        if(firebaseAuth.getCurrentUser() != null){
            //that means user is already logged in
            //so close this activity
            finish();

            //and open profile activity
            startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
        }

        //initializing views
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextPhone = (EditText) findViewById(R.id.editTextMobile);


       btnSignin = (ImageView) findViewById(R.id.buttonSignup);
        btnLogin = (ImageView) findViewById(R.id.buttonLogin);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
       btnSignin.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
       // textViewSignin.setOnClickListener(this);
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void registerUser(){
        if(ConnectivityHelper.isConnectedToNetwork(UserSignUpActivity.this)) {
            //getting email and password from edit texts
            String email = editTextEmail.getText().toString().trim();
            String password = editTextPassword.getText().toString().trim();
            final String mobile = editTextPhone.getText().toString().trim();

            //checking if email and passwords are empty
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Please enter email", Toast.LENGTH_LONG).show();
                return;
            }

            //checking if mobile is empty
            if (TextUtils.isEmpty(mobile)) {
                Toast.makeText(this, "Please enter mobile no", Toast.LENGTH_LONG).show();
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
                return;
            }

            if (!isValidEmail(email)) {
                editTextEmail.setError("Invalid Email");
            }

            if (!isValidPassword(password)) {
                editTextPassword.setError("Invalid Password");
            }
            if (!isValidPhone(mobile)) {
                editTextPhone.setError("Invalid Mobile no");
            }

            //if the email and password are not empty
            //displaying a progress dialog

            progressDialog.setMessage("Registering Please Wait...");
            progressDialog.show();
            //creating a new user
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            //checking if success
                            if (task.isSuccessful()) {
                                FirebaseUser user = firebaseAuth.getCurrentUser();
                                writeNewUser(user.getUid(), getUsernameFromEmail(user.getEmail()), user.getEmail(), mobile);
                                SharedPrefManager.saveEmail(UserSignUpActivity.this, user.getEmail());
                                finish();
                                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                            } else {
                                //display some message here
                                Toast.makeText(UserSignUpActivity.this, R.string.title_registration_error, Toast.LENGTH_LONG).show();
                            }
                            progressDialog.dismiss();

                        }
                    });
        }else {
            Toast.makeText(UserSignUpActivity.this, "Please check internet connection..", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view) {
        if(view == btnSignin){
            registerUser();
        }
        if(view == btnLogin){
            Intent intent = new Intent(UserSignUpActivity.this,UserLoginActivity.class);
            startActivity(intent);
            finish();
        }

      /*  if(view == btnSignin){
            //open login activity when user taps on the already registered textview
            startActivity(new Intent(this, UserLoginActivity.class));
        }*/
    }

    private void writeNewUser(String userId, String username, String email,String mobile) {
        users user = new users(username, email,mobile);

        FirebaseDatabase.getInstance().getReference().child("users").child(userId).setValue(user);
    }
    private String getUsernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }


    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }
    private boolean isValidPhone(String phone)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone))
        {
            if(phone.length() < 10 || phone.length() > 10)
            {
                check = false;

            }
            else
            {
                check = true;

            }
        }
        else
        {
            check=false;
        }
        return check;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
}
