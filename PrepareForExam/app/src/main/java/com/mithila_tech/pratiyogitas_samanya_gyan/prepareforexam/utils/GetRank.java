package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils;

/**
 * Created by ASUS on 4/3/2018.
 */

// Java Code to find rank of elements
public class GetRank {

    // Function to print m Maximum elements
    public static int rankify(int A[], int n,int ele)
    {
        // Rank Vector
        float R[] = new float[n];

        // Sweep through all elements in A
        // for each element count the number
        // of less than and equal elements
        // separately in r and s
        for (int i = 0; i < n; i++) {
            int r = 1, s = 1;

            for (int j = 0; j < n; j++)
            {
                if (j != i && A[j] < A[i])
                    r += 1;

                if (j != i && A[j] == A[i])
                    s += 1;
            }

            // Use formula to obtain  rank
            R[i] = r + (float)(s - 1) / (float) 2;

        }

        //for (int i = 0; i < n; i++)
        //    System.out.print(R[i] + "  ");
            float rank=1;
        for (int i = 0; i < n; i++) {
            if(A[i] == ele)
            {
                rank = R[i];
                break;
            }
        }

        return (int)rank;

    }

    // Driver code
    public static int findRank(int A[],int score)
    {

        int n = A.length;

        for (int i = 0; i < n; i++)
            System.out.print(A[i] + "    ");
        System.out.println();
        return rankify(A, n,score);
    }
}

// This code is contributed by Swetank Modi
