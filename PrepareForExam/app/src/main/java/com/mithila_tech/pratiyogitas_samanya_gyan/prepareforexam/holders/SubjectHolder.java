package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.AvailableTestSeries;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.SubjectActivity;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.TopicActivity;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;

/**
 * Created by ASUS on 12/14/2017.
 */

public class SubjectHolder extends BaseViewHolder{
    private final String TAG = com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.SubjectHolder.class.getSimpleName();

    public TextView categoryTitle;


    public SubjectHolder(final Context context, final View itemView) {
        super(itemView);

        categoryTitle = (TextView)itemView.findViewById(R.id.subject_title);


    }
    public void setItem(Subject post){
        categoryTitle.setText(post.getSname());

    }

    @Override
    public void onBind(int position) {
        super.onBind(position);


    }

    @Override
    protected void clear() {

    }
}