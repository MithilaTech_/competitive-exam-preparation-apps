package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;

/**
 * Created by ASUS on 12/15/2017.
 */

public class TestQuestionHolder extends RecyclerView.ViewHolder {
    private final String TAG = TestQuestionHolder.class.getSimpleName();

    public TextView question,correct_ans,explain,questioncount;
    public RadioButton optA,optB,optC,optD,optE;
    public ImageView qImg,optAImg,optBImg,optCImg,optDImg,optEImg;
    public LinearLayout layoutAns;
    public TextView txtAnswer;


    public TestQuestionHolder(final Context context, final View itemView) {

        super(itemView);

        questioncount = (TextView)itemView.findViewById(R.id.lblquestioncount);
        txtAnswer = (TextView)itemView.findViewById(R.id.lblAnswer) ;
        question = (TextView)itemView.findViewById(R.id.question);
        optA = (RadioButton) itemView.findViewById(R.id.optionA);
        optB = (RadioButton)itemView.findViewById(R.id.optionB);
        optC = (RadioButton)itemView.findViewById(R.id.optionC);
        optD = (RadioButton)itemView.findViewById(R.id.optionD);
        optE = (RadioButton)itemView.findViewById(R.id.optionE);


        qImg = (ImageView) itemView.findViewById(R.id.questionimage);
        optAImg = (ImageView) itemView.findViewById(R.id.optionA_image);
        optBImg = (ImageView) itemView.findViewById(R.id.optionB_image);
        optCImg = (ImageView) itemView.findViewById(R.id.optionC_image);
        optDImg = (ImageView) itemView.findViewById(R.id.optionD_image);
        optEImg = (ImageView) itemView.findViewById(R.id.optionE_image);






    }
}
