package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;

/**
 * Created by ASUS on 12/15/2017.
 */

public class QuestionHolder extends RecyclerView.ViewHolder {
    private final String TAG = com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.QuestionHolder.class.getSimpleName();

    public TextView question,correct_ans,explain,questioncount;
    public Button optA,optB,optC,optD,optE;
    public ImageView qImg,optAImg,optBImg,optCImg,optDImg,optEImg,correct_ans_img,exp_img;
    public LinearLayout layoutAns;
    public TextView txtAnswer;


    public QuestionHolder(final Context context, final View itemView) {

        super(itemView);
        layoutAns = (LinearLayout)itemView.findViewById(R.id.layoutAnswer);
        layoutAns.setVisibility(View.GONE);
        questioncount = (TextView)itemView.findViewById(R.id.lblquestioncount);
        txtAnswer = (TextView)itemView.findViewById(R.id.lblAnswer) ;
        question = (TextView)itemView.findViewById(R.id.question);
        optA = (Button) itemView.findViewById(R.id.optionA);
        optB = (Button)itemView.findViewById(R.id.optionB);
        optC = (Button)itemView.findViewById(R.id.optionC);
        optD = (Button)itemView.findViewById(R.id.optionD);
        optE = (Button)itemView.findViewById(R.id.optionE);
        correct_ans = (TextView)itemView.findViewById(R.id.answer);
        explain = (TextView)itemView.findViewById(R.id.explaination);

        qImg = (ImageView) itemView.findViewById(R.id.questionimage);
        optAImg = (ImageView) itemView.findViewById(R.id.optionA_image);
        optBImg = (ImageView) itemView.findViewById(R.id.optionB_image);
        optCImg = (ImageView) itemView.findViewById(R.id.optionC_image);
        optDImg = (ImageView) itemView.findViewById(R.id.optionD_image);
        optEImg = (ImageView) itemView.findViewById(R.id.optionE_image);
       correct_ans_img = (ImageView) itemView.findViewById(R.id.answer_image);
        exp_img = (ImageView) itemView.findViewById(R.id.expl_image);





    }
}
