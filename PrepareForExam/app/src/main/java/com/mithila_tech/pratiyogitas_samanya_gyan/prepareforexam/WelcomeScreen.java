package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;

public class WelcomeScreen extends AppCompatActivity  implements
        AdapterView.OnItemSelectedListener  {

    String[] language= { "Hindi"};
    TextView usernm;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;

    //    ImageView imageViewStart,imageViewMore,imageViewShare, imageViewLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =  getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_HIGH));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);//initializing firebase authentication object
        Spinner spin = (Spinner) findViewById(R.id.spinner1);
        spin.setOnItemSelectedListener(this);
        usernm = (TextView) findViewById(R.id.lblusername);
        usernm.setText(SharedPrefManager.getUserEmail(WelcomeScreen.this));
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,language);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);
       /* MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());*/
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("FirebaseIns", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.e("FirebaseIns", msg);
                        Log.e("Message", msg);
                    }
                });

    }

    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.imageView_Rating:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.mithila_tech.madhyapradeshrojgarsamachar"));
                startActivity(intent);
                Toast.makeText(this, " Rating clicked.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imageView_Share:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                String ShareBody = "''Pratiyogita SamanyGyan Darpan'' is the first dedicated App Launched in Madhya Pradesh (India) which provides" +
                        " information of M.P. Government recruitments. In this app users can get all the latest updates relates with all types " +
                        "of government jobs in Chhattisgarh such as M.P.Vyapam,District Recruitment ,Mantralaya Recruitment ,MPPSC,CGPSC, IBPS, Indian Army," +
                        "UPSC, SSC, RRB, Railway_Jobs, etc. and various State government jobs. in Hindi and English as well.\n" +
                        "Download this app and get maximum benefit to boost your career. Share via Facebook, Twitter, GooglePlus, WhatsApp....Share with friends!\n" +
                        "Download the App Now : http://bit.ly/2vH4io1\n";
                String ShareSub = "Madhya Pradesh Rojgar Samachar Share - ";
                shareIntent.putExtra(Intent.EXTRA_SUBJECT,ShareSub);
                shareIntent.putExtra(Intent.EXTRA_TEXT,ShareBody);
                startActivity(Intent.createChooser(shareIntent,"Share using"));
                break;
            case R.id.imageView_Logout:
                //Inform the user the button2 has been clicked
                //Toast.makeText(this, "Button2 clicked.", Toast.LENGTH_SHORT).show();
                ProfileActivity.logoutuser(WelcomeScreen.this);
                break;
            case R.id.imageView_More:
                Intent intent1 = new Intent(WelcomeScreen.this,More_Screen_welcome.class);
                startActivity(intent1);
                break;
        }

    }

    public void dismisWelcomeMessageBox(View view) {
        Intent i = new Intent(WelcomeScreen.this,UserLoginActivity.class);
        startActivity(i);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        SharedPrefManager.setLang(WelcomeScreen.this,language[i]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * Display an force update dialog on any new version updates for the app
     */
    public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

        private String latestVersion;
        private String currentVersion;
        private Context context;
        public ForceUpdateAsync(String currentVersion, Context context){
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try {
                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id="+context.getPackageName()+"&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(latestVersion!=null){
                if(!currentVersion.equalsIgnoreCase(latestVersion)){
                    // Toast.makeText(context,"update is available.",Toast.LENGTH_LONG).show();
                    if(!(context instanceof WelcomeScreen)) {
                        if(!((Activity)context).isFinishing()){
                            showForceUpdateDialog();
                        }
                    }
                }
            }
            super.onPostExecute(jsonObject);
        }

        public void showForceUpdateDialog(){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context,
                    R.style.Theme_AppCompat_Light_Dialog));

            alertDialogBuilder.setTitle(context.getString(R.string.youAreNotUpdatedTitle));
            alertDialogBuilder.setMessage(context.getString(R.string.youAreNotUpdatedMessage) + " " + latestVersion + context.getString(R.string.youAreNotUpdatedMessage1));
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                    dialog.cancel();
                }
            });
            alertDialogBuilder.show();
        }
    }

    // check version on play store and force update
    public void forceUpdate(){
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion,WelcomeScreen.this).execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        forceUpdate();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    /*    if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }*/
    }
}
