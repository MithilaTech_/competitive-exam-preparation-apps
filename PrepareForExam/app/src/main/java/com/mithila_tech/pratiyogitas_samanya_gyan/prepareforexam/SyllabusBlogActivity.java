package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class SyllabusBlogActivity extends AppCompatActivity {

    private WebView mBlog;
    private String mUrl;
    private String ctitle;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus_blog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Syllabus Blog");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();
        if(i!=null) {
            mUrl = i.getStringExtra("burl");
            if(mUrl==null || mUrl.length()<=0) {
                mUrl = "http://www.google.com";
            }
            ctitle =i.getStringExtra("stitle");
            if(ctitle==null || ctitle.length()<=0) {
                ctitle =SyllabusBlogActivity.class.getSimpleName();
            }
        }
        getSupportActionBar().setTitle(ctitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mBlog = (WebView) findViewById(R.id.syllabusBlog);
        mBlog.getSettings().setJavaScriptEnabled(true);
        mBlog.getSettings().setPluginState(WebSettings.PluginState.ON);
        mBlog.setWebViewClient(new Callback());
        mBlog.loadUrl(
                "http://docs.google.com/gview?embedded=true&url=" +mUrl);
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
