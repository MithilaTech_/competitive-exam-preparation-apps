package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.scores;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.TestSeries;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.GetRank;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.github.deweyreed.digitalwatchview.DigitalWatchView;

public class TestSeriesQuestionsActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private LinearLayout layAnswer;
    private static final String TAG = StudyQuestionActivity.class.getSimpleName();

    private DatabaseReference databaseReference,quesreference;
    private String tname,topicname,subjectname,commontestname;
    private ImageLoader imageLoader;
    private ArrayList<TestSeries> allQuestions;
    private ProgressDialog mProgressBar;
    private String q_language;
    private int count=0;
    private Query query;
    private  LayoutInflater inflater;    //Used to create individual pages
    private ViewPager vp;
    private int qcount =0;
    public RadioButton optA,optB,optC,optD,optE;
    public RadioGroup r1;
    private DigitalWatchView txtHour, txtMinute, txtSecond;
    private TextView tvEventStart;
    private Handler handler;
    private Runnable runnable;
    private long hours,minutes,seconds;
    private CountDownTimer testTimer;
    private Button btnSubmit;
    private String ansVal;
    private int score=0;
    private Button btnNext,btnPrevious;
    private boolean subjectwise=false;
    private boolean istopictestseries,issubjecttestseries,iscommontestseries;
    private final HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    private int attempted =0 , unattempted =0;
    private int user_scores [];
    int countchecks=0;
    private int rank;
    private int wrongans=0;
    private InterstitialAd mInterstitialAd;
    private final ArrayList scorelist = new ArrayList();
    private ArrayList alist = new ArrayList();
    private ArrayList unalist = new ArrayList();
    private final HashMap<Integer, Integer> wronglist = new HashMap<Integer, Integer>();
    private final HashMap<Integer, Integer> rightlist = new HashMap<Integer, Integer>();
    private int pos=0;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_series_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //get an inflater to be used to create single pages

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.keepSynced(true);

        databaseReference.keepSynced(false);
        quesreference = databaseReference.child("/test_series");
        if(intent !=null) {
            topicname = intent.getStringExtra("topicname");
            subjectname = intent.getStringExtra("subjectname");
            commontestname = intent.getStringExtra("commontestname");
            istopictestseries = intent.getBooleanExtra("istopictestseries",false);
            issubjecttestseries = intent.getBooleanExtra("issubjecttestseries",false);
            iscommontestseries = intent.getBooleanExtra("iscommontestseries",false);

                tname = intent.getStringExtra("topicid");
                if(tname ==null || tname.length()<=0) {
                    tname = "भारतीय अर्थव्यवस्था";
                }
                if(istopictestseries) {
                    query = quesreference.orderByChild("topic_test_id").equalTo(tname);
                    getSupportActionBar().setTitle("Topic Test Series No:"+tname);


                } else if(issubjecttestseries) {
                    query = quesreference.orderByChild("sub_test_id").equalTo(tname);
                    getSupportActionBar().setTitle("Subject Test Series No:"+tname);
                } else {
                    query = quesreference.orderByChild("common_test_id").equalTo(tname);
                    getSupportActionBar().setTitle("Common Test Series No:"+tname);
                }

        }


        imageLoader = ImageLoader.getInstance();
        q_language = SharedPrefManager.getLang(TestSeriesQuestionsActivity.this);
        vp=(ViewPager)findViewById(R.id.viewPager);
        vp.addOnPageChangeListener(this);
       /* txtHour = (TextView) findViewById(R.id.txt_Hour);
        txtMinute = (TextView) findViewById(R.id.txt_Minute);
        txtSecond = (TextView) findViewById(R.id.txt_Second);*/
        txtHour = (DigitalWatchView) findViewById(R.id.digitalWatchView);

        allQuestions= new ArrayList<TestSeries>();
        mProgressBar = new ProgressDialog(TestSeriesQuestionsActivity.this);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setCancelable(false);
        mProgressBar.setMessage(getString(R.string.title_loading));
        mProgressBar.show();

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //Log.e("SubjectActivity","onChildAdded()");
                HashMap<String,String> obj= (HashMap<String, String>) dataSnapshot.getValue();

                //if(qcount<=20) {
                    map.put(countchecks++,null);
                    wronglist.put(countchecks++,null);
                    rightlist.put(countchecks++,null);
                    String comm_Test_ID = (String) obj.get("common_test_id");
                    String c_ans = (String) obj.get("Correct_Ans_Text");
                    String c_ans_url = (String) obj.get("Correct_Ans_Image_url");
                    String exp = (String) obj.get("Explaination_Test");
                    String exp_url = (String) obj.get("Explanation_Image_url");
                    String qlang = (String) obj.get("language");
                    String optAurl = (String) obj.get("Option_A_Image_url");
                    String optA = (String) obj.get("Option_A_text");
                    String optBurl = (String) obj.get("Option_B_Image_url");
                    String optB = (String) obj.get("Option_B_text");
                    String optCurl = (String) obj.get("Option_C_Image_url");
                    String optC = (String) obj.get("Option_C_text");
                    String optDurl = (String) obj.get("Option_D_Image_url");
                    String optD = (String) obj.get("Option_D_text");
                    String optEurl = (String) obj.get("Option_E_Image_url");
                    String optE = (String) obj.get("Option_E_text");
                    String ques = (String) obj.get("question");
                    String quesimgurl = (String) obj.get("Questions_Image_url");
                    String subtestid = (String) obj.get("sub_test_id");
                    String subname = (String) obj.get("subjectname");
                    String timeinminutes = (String) obj.get("timeinminutes");
                    String topname = (String) obj.get("topcname");
                    String topic_test_id = (String) obj.get("topic_test_id");


                    TestSeries quest = new TestSeries();
                    quest.setCommon_test_id(comm_Test_ID);
                    quest.setCorrect_ans(c_ans);
                    quest.setCorrect_Ans_Image_url(c_ans_url);
                    quest.setExplaination(exp);
                    quest.setExplanation_Image_url(exp_url);
                    quest.setLanguage(qlang);



                    quest.setOption_A_Image_url(optAurl);
                    quest.setOption_A_text(optA);
                    quest.setOption_B_Image_url(optBurl);
                    quest.setOption_B_text(optB);
                    quest.setOption_C_Image_url(optCurl);
                    quest.setOption_C_text(optC);
                    quest.setOption_D_Image_url(optDurl);
                    quest.setOption_D_text(optD);
                    quest.setOption_E_Image_url(optEurl);
                    quest.setOption_E_text(optE);
                    quest.setQuestion(ques);
                    quest.setQuestions_Image_url(quesimgurl);
                    quest.setSub_test_id(subtestid);
                    quest.setSubjectname(subname);
                    quest.setTimeinminutes(timeinminutes);
                    quest.setTopcname(topname);
                    quest.setTopic_test_id(topic_test_id);
                    allQuestions.add(quest);
                    qcount++;
                //}

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(allQuestions!=null && allQuestions.size()>0) {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();
                        //Reference ViewPager defined in activity

                        //set the adapter that will create the individual pages
                        vp.setOffscreenPageLimit(3);

                        vp.setAdapter(new MyPagesAdapter());
                        long timemin = Long.parseLong(allQuestions.get(0).getTimeinminutes());
                        timemin = TimeUnit.MINUTES.toMillis(timemin);
                        testTimer = new TestTimer(timemin,1000) ;
                        testTimer.start();

                    }
                    qcount=0;


                } else {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }
                    qcount=0;
                    if(testTimer!=null) {
                        testTimer.cancel();
                    }
                    Toast.makeText(TestSeriesQuestionsActivity.this, "No questions found...", Toast.LENGTH_SHORT).show();
                    finish();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


    public void onNavigateClick(View view) {

        switch(view.getId()) {
            case R.id.btnNext:
              /*  index++;
                if(index>=allQuestions.size()) {
                    index = allQuestions.size()-1;
                }*/
                vp.setCurrentItem(vp.getCurrentItem() + 1);
                //index= vp.getCurrentItem()+1;

                break;
            case R.id.btnPrevious:
              /*  index--;
                if(index<0) {
                    index = 0;
                }*/
                vp.setCurrentItem(vp.getCurrentItem()-1);
               // index= vp.getCurrentItem()-1;
                break;
            case R.id.btnSubmit:
               // Toast.makeText(TestSeriesQuestionsActivity.this,"Submit",Toast.LENGTH_SHORT).show();
                checkUnattempted();
                checkAndUpdateScore();


                break;
        }
    }

    public void showResult() {
        Intent i = new Intent(getApplicationContext(), ShareActivity.class);
        i.putExtra("isTestSeries", true);
        i.putExtra("outof",allQuestions.size());
        score=0;
        Log.e("Scores","Score Size:"+rightlist.size());

        for(int ii=0;ii<rightlist.size();ii++) {
            if (rightlist.get(ii) != null) {
                score += Integer.parseInt(rightlist.get(ii).toString());
            }
        }
        attempted=0;
        for(int ii=0;ii<alist.size();ii++) {
            attempted += Integer.parseInt(alist.get(ii).toString());
        }
        unattempted=0;
        for(int ii=0;ii<unalist.size();ii++) {
            unattempted += Integer.parseInt(unalist.get(ii).toString());
        }
        wrongans=0;
        for(int ii=0;ii<wronglist.size();ii++) {
            if (wronglist.get(ii) != null) {
                wrongans += Integer.parseInt(wronglist.get(ii).toString());
            }
        }
        i.putExtra("score",score);
        i.putExtra("attempted",attempted);
        i.putExtra("unattempted",unattempted);
        i.putExtra("wrongans",wrongans);
        //i.putExtra("rank",rank);
        i.putParcelableArrayListExtra("questionlist", allQuestions);
        Toast.makeText(TestSeriesQuestionsActivity.this,"Submit",Toast.LENGTH_SHORT).show();
        this.startActivity(i);
        finish();
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

     /*   if(!(optA.isChecked())&&!(optB.isChecked())&&!(optC.isChecked())&&!(optD.isChecked())&&!(optE.isChecked())) {
            unattempted++;
        }*/







    }

    @Override
    public void onPageSelected(int position) {
        if(position >= allQuestions.size()-1) {
            btnSubmit.setEnabled(true);
            btnPrevious.setEnabled(true);
            btnNext.setEnabled(false);


        }
        else {
            btnSubmit.setEnabled(false);
            btnPrevious.setEnabled(true);
            btnNext.setEnabled(true);

        }

        if(position ==0) {
            btnSubmit.setEnabled(false);
            btnPrevious.setEnabled(false);
            btnNext.setEnabled(true);
        }
      /*  if(index>position) {
            int diff = index-position;
            index-=diff;
        } else if (index<position) {
            int diff = position-index;
            index+=diff;
        }*/

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    //Implement PagerAdapter Class to handle individual page creation
    class MyPagesAdapter extends PagerAdapter {

        public TextView question,correct_ans,explain,questioncount;

        public ImageView qImg,optAImg,optBImg,optCImg,optDImg,optEImg,correct_ans_img,exp_img;
        public LinearLayout layoutAns;
        public TextView txtAnswer;
        public MyPagesAdapter() {
            fillArrays();
        }
        @Override
        public int getCount() {
            //Return total pages, here one for each data item
            return allQuestions.size();

        }
        //Create the given page (indicated by position)
        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.test_series_question_list, null);


            questioncount = (TextView)itemView.findViewById(R.id.lblquestioncount);
            btnNext = (Button) itemView.findViewById(R.id.btnNext);
            btnNext.setVisibility(View.VISIBLE);
            btnPrevious = (Button) itemView.findViewById(R.id.btnPrevious);
            btnPrevious.setVisibility(View.VISIBLE);
            btnSubmit = (Button) itemView.findViewById(R.id.btnSubmit);

            txtAnswer = (TextView)itemView.findViewById(R.id.lblAnswer) ;
            question = (TextView)itemView.findViewById(R.id.question);
            r1 = (RadioGroup)itemView.findViewById(R.id.radioGroup1);
            optA = (RadioButton) itemView.findViewById(R.id.optionA);
            optB = (RadioButton)itemView.findViewById(R.id.optionB);
            optC = (RadioButton)itemView.findViewById(R.id.optionC);
            optD = (RadioButton)itemView.findViewById(R.id.optionD);
            optE = (RadioButton)itemView.findViewById(R.id.optionE);


            qImg = (ImageView) itemView.findViewById(R.id.questionimage);
            optAImg = (ImageView) itemView.findViewById(R.id.optionA_image);
            optBImg = (ImageView) itemView.findViewById(R.id.optionB_image);
            optCImg = (ImageView) itemView.findViewById(R.id.optionC_image);
            optDImg = (ImageView) itemView.findViewById(R.id.optionD_image);
            optEImg = (ImageView) itemView.findViewById(R.id.optionE_image);

           //index = position;
            if (position < allQuestions.size()) {
            Log.e("Position","Position1:"+position);
                final TestSeries q = allQuestions.get(position);
                //if (q.getTopcname().equals(tname)) {
               questioncount.setText(String.valueOf(position+1)+".");
               question.setText(q.getQuestion());
                if (q.getQuestions_Image_url()!=null && !q.getQuestions_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                    qImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(q.getQuestions_Image_url(), qImg);
                }
                optA.setText(q.getOption_A_text());
                if (q.getOption_A_Image_url()!=null && !q.getOption_A_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                    optAImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(q.getOption_A_Image_url(), optAImg);
                }
               optB.setText(q.getOption_B_text());
                if (q.getOption_B_Image_url()!=null && !q.getOption_B_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                    optBImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(q.getOption_B_Image_url(), optBImg);
                }
               optC.setText(q.getOption_C_text());
                if (q.getOption_C_Image_url()!=null && !q.getOption_C_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                    optCImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(q.getOption_C_Image_url(), optCImg);
                }
                optD.setText(q.getOption_D_text());
                if (!q.getOption_D_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                    optDImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(q.getOption_D_Image_url(), optDImg);
                }
                optE.setText(q.getOption_E_text());
                if (q.getOption_E_Image_url()!=null && !q.getOption_E_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
                    optEImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(q.getOption_E_Image_url(),optEImg);
                }

             /*   if(index>=allQuestions.size()) {
                    index = allQuestions.size()-1;
                }*/

                if(map.get(position) !=null) {
                    int id = map.get(index);
                    setCheckedId(id);
                    //  vp.getAdapter().notifyDataSetChanged();
                }
                r1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                   @Override
                   public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                     //  if((map.get(pos)==null)) {
                           RadioButton rb = (RadioButton) group.findViewById(checkedId);
                           if(rb.isChecked()) {
                               if(map.get(position) ==null) {
                                   map.put(position, checkedId);
                               }
                               boolean rightans =false;
                               // if(!(map.get(vp.getCurrentItem())==null)) {
                               //score=0;

                               switch (checkedId) {
                                   case R.id.optionA:
                                       ansVal = "A";
                                       if (ansVal.equals(q.getCorrect_ans())) {

                                          // if (rightlist.get(position) == null) {
                                               rightlist.put(position, 1);
                                           if (wronglist.get(position) != null) {
                                               wronglist.remove(position);
                                           }
                                            //   }
                                       } else {
                                         //  if (wronglist.get(position) == null) {
                                           wronglist.put(position, 1);
                                           if (rightlist.get(position) != null) {
                                               rightlist.remove(position);
                                           }
                                       }

                                       break;
                                   case R.id.optionB:
                                       ansVal = "B";
                                       if (ansVal.equals(q.getCorrect_ans())) {

                                           // if (rightlist.get(position) == null) {
                                           rightlist.put(position, 1);
                                           if (wronglist.get(position) != null) {
                                               wronglist.remove(position);
                                           }
                                           //   }
                                       } else {
                                           //  if (wronglist.get(position) == null) {
                                           wronglist.put(position, 1);
                                           if (rightlist.get(position) != null) {
                                               rightlist.remove(position);
                                           }
                                       }

                                       break;
                                   case R.id.optionC:
                                       ansVal = "C";

                                       if (ansVal.equals(q.getCorrect_ans())) {
                                           // if (rightlist.get(position) == null) {
                                           rightlist.put(position, 1);
                                           if (wronglist.get(position) != null) {
                                               wronglist.remove(position);
                                           }
                                           //   }
                                       } else {
                                           //  if (wronglist.get(position) == null) {
                                           wronglist.put(position, 1);
                                           if (rightlist.get(position) != null) {
                                               rightlist.remove(position);
                                           }
                                       }

                                       break;
                                   case R.id.optionD:
                                       ansVal = "D";
                                       if (ansVal.equals(q.getCorrect_ans())) {
                                           // if (rightlist.get(position) == null) {
                                           rightlist.put(position, 1);
                                           if (wronglist.get(position) != null) {
                                               wronglist.remove(position);
                                           }
                                           //   }
                                       } else {
                                           //  if (wronglist.get(position) == null) {
                                           wronglist.put(position, 1);
                                           if (rightlist.get(position) != null) {
                                               rightlist.remove(position);
                                           }
                                       }


                                       break;
                                   case R.id.optionE:
                                       ansVal = "E";
                                       if (ansVal.equals(q.getCorrect_ans())) {
                                           // if (rightlist.get(position) == null) {
                                           rightlist.put(position, 1);
                                           if (wronglist.get(position) != null) {
                                               wronglist.remove(position);
                                           }
                                           //   }
                                       } else {
                                           //  if (wronglist.get(position) == null) {
                                           wronglist.put(position, 1);
                                           if (rightlist.get(position) != null) {
                                               rightlist.remove(position);
                                           }
                                       }

                                       break;
                                   default:
                                       rightans=false;


                               }
                              /* if(!rightans){
                                   //if (wronglist.get(position) == null) {
                                       wronglist.put(position, 1);
                                  // }
                               } else {
                                  // if (rightlist.get(position) == null) {
                                       rightlist.put(position, 1);
                                   //}

                               }*/

                               // attempted++;
                               //  alist.add(attempted);

                               // }
                           // vp.getAdapter().notifyDataSetChanged();
                           }
                      // }

                   }});
              /*  index++;*/

            }
            ((ViewPager) container).addView(itemView, 0);
            return itemView;
        }
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            //See if object from instantiateItem is related to the given view
            //required by API
            return arg0==(View)arg1;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
            object=null;
        }
    }

   class TestTimer extends CountDownTimer {

       public TestTimer(long startTime, long interval) {
           super(startTime, interval);
       }

       @Override
       public void onTick(long millisUntilFinished) {
           long totalSecs = millisUntilFinished/1000;
           long hours = (totalSecs / 3600);
           long mins = (totalSecs / 60) % 60;
           long secs = totalSecs % 60;
           String minsString = (mins == 0)
                   ? "00"
                   : ((mins < 10)
                   ? "0" + mins
                   : "" + mins);
           String secsString = (secs == 0)
                   ? "00"
                   : ((secs < 10)
                   ? "0" + secs
                   : "" + secs);

          /* txtHour.setText(String.valueOf(hours));
           txtMinute.setText(minsString);
           txtSecond.setText(secsString);*/
           txtHour.setHours((int) hours);
           txtHour.setMinutes(Integer.parseInt(minsString));
           txtHour.setSeconds(Integer.parseInt(secsString));


       }

       @Override
       public void onFinish() {
           btnNext.setVisibility(View.GONE);
           btnPrevious.setVisibility(View.GONE);
           btnSubmit.setVisibility(View.VISIBLE);
           txtHour.setHours(0);
           txtHour.setMinutes(0);
           txtHour.setSeconds(0);
       }
   }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setCheckedId(int checkedId) {
        switch(checkedId) {
            case R.id.optionA :
                optA.setChecked(true);
                optB.setChecked(false);
                optC.setChecked(false);
                optD.setChecked(false);
                optE.setChecked(false);
                break;
            case R.id.optionB :
                optA.setChecked(false);
                optB.setChecked(true);
                optC.setChecked(false);
                optD.setChecked(false);
                optE.setChecked(false);
                break;
            case R.id.optionC :
                optA.setChecked(false);
                optB.setChecked(false);
                optC.setChecked(true);
                optD.setChecked(false);
                optE.setChecked(false);
                break;
            case R.id.optionD :
                optA.setChecked(false);
                optB.setChecked(false);
                optC.setChecked(false);
                optD.setChecked(true);
                optE.setChecked(false);
                break;
            case R.id.optionE :
                optA.setChecked(false);
                optB.setChecked(false);
                optC.setChecked(false);
                optD.setChecked(false);
                optE.setChecked(true);
                break;
        }

    }
    String ids = "";
    public void checkAndUpdateScore() {

        if(databaseReference.child("/scores")!=null) {

            final DatabaseReference myRef = databaseReference.child("/scores");
            ids = "";
            if (subjectname.equals("NA") && topicname.equals("NA") && commontestname.equals("common")) {
                ids = "NA" + "-" + "NA" + "-" + tname;
            } else if (topicname.equals("NA") && commontestname.equals("NA")) {
                ids = tname + "-" + "NA" + "-" + "NA";
            } else if (subjectname.equals("NA") && commontestname.equals("NA")) {
                ids = "NA" + "-" + tname + "-" + "NA";
            }


            Query query1 = myRef.orderByChild("uname_detail").equalTo(SharedPrefManager.getUserEmail(TestSeriesQuestionsActivity.this) + "-" + subjectname + "-" + topicname + "-" + commontestname + "-" + ids).limitToLast(1);
            final TaskCompletionSource<scores> taskSource = new TaskCompletionSource<scores>();

            query1.addListenerForSingleValueEvent(new ValueEventListener() {


                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Log.e("TestSeries", "dddfff" + SharedPrefManager.getUserEmail(TestSeriesQuestionsActivity.this) + "-" + subjectname + "-" + topicname + "-" + commontestname + "-" + ids);
                    int c=0;
                    for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {

                        c++;
                         scores obj = childSnapshot.getValue(scores.class);
                         taskSource.setResult(obj);

                    }
                    if(c==0) {
                        Log.e("TestSeries", "ssssss" + "else");
                        saveScore();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            Task task = taskSource.getTask();
            task.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {


                        scores ss1 = (scores) task.getResult();
                        String sc = String.valueOf(ss1.getScore());
                        if (sc != null) {

                            String tdetail = ss1.getTest_detail();
                            String uid = ss1.getUid();
                            String uname = ss1.getUname();
                            String udetail = ss1.getUname_detail();
                            scores sc1 = new scores();
                            sc1.setUid(uid);
                             Log.e("TestSeries","ddd"+sc1.getUid());
                            sc1.setUname(uname);
                            sc1.setUname_detail(udetail);
                            sc1.setTest_detail(tdetail);

                            sc1.setScore(Integer.parseInt(nulltoIntegerDefalt(sc)));

                            updateScore(sc1);

                        } else {
                            Log.e("TestSeries", "ssssss" + "else");
                            saveScore();
                        }
                    }
                }

            });

        }


    }
    String nulltoIntegerDefalt(String value) {
        if (!isIntValue(value)) value = "0";
        return value;
    }
    boolean isIntValue(String val)
    {
        try {
            val=val.replace(" ","");
            Integer.parseInt(val);
        } catch (Exception e) {return false;}
        return true;
    }
public void saveScore() {

    scores testscore = new scores();
    String tdetail = "";
    String ids = "";
    testscore.setUid(databaseReference.child("scores").push().getKey());
    if(subjectname.equals("NA") && topicname.equals("NA")&& commontestname.equals("common")) {
        ids="NA"+"-"+"NA"+"-"+tname;
    }else if( topicname.equals("NA")&& commontestname.equals("NA")) {
        ids=tname+"-"+"NA"+"r-"+"NA";
    }else if( subjectname.equals("NA")&& commontestname.equals("NA")) {
        ids="NA"+"-"+tname+"-"+"NA";
    }

    testscore.setUname(SharedPrefManager.getUserEmail(TestSeriesQuestionsActivity.this));
    tdetail = SharedPrefManager.getUserEmail(TestSeriesQuestionsActivity.this)+"-"+subjectname+"-"+topicname+"-"+commontestname+"-";
    tdetail+=ids;
    testscore.setUname_detail(tdetail);
    showResult();




    testscore.setTest_detail(subjectname+"-"+topicname+"-"+commontestname+"-"+ids);
    score=0;
    for(int i=0;i<rightlist.size();i++) {
        score += Integer.parseInt(rightlist.get(i).toString());
    }
    testscore.setScore(score);
    databaseReference.child("scores").child(testscore.getUid()).setValue(testscore);
    //rank = getRanksOnScores();

}

    public void updateScore(scores testscore) {
        final DatabaseReference myRef = databaseReference.child("/scores");
        myRef.child(testscore.getUid()).child("score").setValue(score);
        showResult();
       // rank = getRanksOnScores();
     /*
        String tdetail = "";
        String ids = "";
        testscore.setUid(databaseReference.child("scores").push().getKey());
        testscore.setUname(SharedPrefManager.getUserEmail());
        tdetail = subjectname+"-"+topicname+"-"+commontestname+"-";
        if(subjectname.equals("NA") && topicname.equals("NA")&& commontestname.equals("common")) {
            ids="NA"+"-"+"NA"+"-"+tname;
        }else if( topicname.equals("NA")&& commontestname.equals("NA")) {
            ids=tname+"-"+"NA"+"-"+"NA";
        }else if( subjectname.equals("NA")&& commontestname.equals("NA")) {
            ids="NA"+"-"+tname+"-"+"NA";
        }
        tdetail+=ids;
        testscore.setTest_detail(tdetail);
        testscore.setScore(score);*/


    }
   static int index =0;
    public int getRanksOnScores() {
        final DatabaseReference myRef = databaseReference.child("/scores");
        String ids = "";
        index=0;
        if(subjectname.equals("NA") && topicname.equals("NA")&& commontestname.equals("common")) {
            ids="NA"+"-"+"NA"+"-"+tname;
        }else if( topicname.equals("NA")&& commontestname.equals("NA")) {
            ids=tname+"-"+"NA"+"-"+"NA";
        }else if( subjectname.equals("NA")&& commontestname.equals("NA")) {
            ids="NA"+"-"+tname+"-"+"NA";
        }
        Query query1 = myRef.orderByChild("test_detail").equalTo(subjectname+"-"+topicname+"-"+commontestname+"-"+ids);
        query1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user_scores = new int[(int)dataSnapshot.getChildrenCount()];
                if(dataSnapshot.getChildrenCount()>0) {
                    HashMap<String,String> ss= (HashMap<String, String>) dataSnapshot.getValue();

                    String sc = ss.get("score");
                    user_scores[index++] = Integer.parseInt(sc);

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
            return GetRank.findRank(user_scores,score);

    }

    public void checkUnattempted() {
        unattempted=0;
        for (int i=0;i<allQuestions.size();i++) {
            if(map.get(i)==null) {
                unalist.set(i,1);
            } else {
                alist.set(i,1);
            }
        }
    }
    public void fillArrays() {
      /*  if(scorelist.size()>0) {
            scorelist.clear();
        }
        if(wronglist.size()>0) {
            wronglist.clear();
        }
        if(rightlist.size()>0) {
            rightlist.clear();
        }
        if(alist.size()>0) {
            alist.clear();
        }
        if(unalist.size()>0) {
            unalist.clear();
        }*/
        if(allQuestions!=null && allQuestions.size()>0) {
            //scorelist = new ArrayList();
            alist = new ArrayList(allQuestions.size());
            unalist = new ArrayList(allQuestions.size());
          /*  wronglist = new ArrayList(allQuestions.size());
            rightlist = new ArrayList(allQuestions.size());*/
            for(int i=0;i<allQuestions.size();i++) {
               // scorelist.add(i,0);
                alist.add(i,0);
                unalist.add(i,0);
                /*wronglist.add(i,0);
                rightlist.add(i,0);*/
            }
        }


    }


}
