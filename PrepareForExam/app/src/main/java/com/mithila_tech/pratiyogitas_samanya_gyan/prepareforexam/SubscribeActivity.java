package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.notifications.helper.MySharedPreference;

public class SubscribeActivity extends AppCompatActivity {
    private static final String TAG = SubscribeActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static MySharedPreference mySharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getLayoutInflater().inflate(R.layout.settings_toolbar, (ViewGroup) findViewById(android.R.id.content));
        setContentView(R.layout.settings_toolbar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setTitle("Subscribe");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Subscribe");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mySharedPreference = new MySharedPreference(getApplicationContext());
        getFragmentManager().beginTransaction()
                .replace(R.id.frameLayout, new GeneralPreferenceFragment())
                .commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            Preference notificationPref = (SwitchPreference)findPreference("upsc");
            notificationPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean isNotificationOn = (Boolean) newValue;
                    if(preference.getKey().equals("upsc") && isNotificationOn){
                        // call the push registration for this user
                        checkPlayServices();
                        FirebaseMessaging.getInstance().subscribeToTopic("upsc");
                        String token = FirebaseInstanceId.getInstance().getToken();
                        Log.d("Subscribe", "Token value " + token);
                        if (token!=null){
                            mySharedPreference.saveNotificationSubscription(true);
                        } else {
                            mySharedPreference.saveNotificationSubscription(false );
                        }
                    }else{
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("upsc");
                    }
                    return true;
                }
            });
        }
        private boolean checkPlayServices() {
            GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
            int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
            if (resultCode != ConnectionResult.SUCCESS) {
                if (apiAvailability.isUserResolvableError(resultCode)) {
                    apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Log.i("PlayServices", "This device is not supported.");
                    getActivity().finish();
                }
                return false;
            }
            return true;
        }
    }
}
