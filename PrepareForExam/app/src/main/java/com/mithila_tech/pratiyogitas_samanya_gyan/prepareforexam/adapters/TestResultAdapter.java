package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.TestSeries;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utilities.AnimationUtility;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by ASUS on 2/18/2018.
 */

public class TestResultAdapter extends RecyclerView.Adapter {
    private final Context context;
    private List<TestSeries> values;
    private ImageLoader imageLoader;

    public void add(int position,TestSeries item) {
        values.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TestResultAdapter(Context context, ImageLoader loader, List<TestSeries> myDataset) {
        this.context = context;
        this.imageLoader = loader;
        values = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                context);
        View v =
                inflater.inflate(R.layout.content_result, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        // if (position < values.size()) {
        final ViewHolder h = (ViewHolder)holder;
        final TestSeries q = values.get(position);
        //if (q.getTopcname().equals(tname)) {
        h.questioncount.setText(String.valueOf(position+1)+".");
        h.question.setText(q.getQuestion());
        if (q.getQuestions_Image_url()!=null && !q.getQuestions_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.qImg.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getQuestions_Image_url(), h.qImg);
        }
        h.optA.setText(q.getOption_A_text());
        if (q.getOption_A_Image_url()!=null && !q.getOption_A_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.optAImg.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getOption_A_Image_url(), h.optAImg);
        }
        h.optB.setText(q.getOption_B_text());
        if (q.getOption_B_Image_url()!=null && !q.getOption_B_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.optBImg.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getOption_B_Image_url(), h.optBImg);
        }
        h.optC.setText(q.getOption_C_text());
        if (q.getOption_C_Image_url()!=null && !q.getOption_C_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.optCImg.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getOption_C_Image_url(), h.optCImg);
        }
        h.optD.setText(q.getOption_D_text());
        if (!q.getOption_D_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
           h.optDImg.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getOption_D_Image_url(), h.optDImg);
        }
        h.optE.setText(q.getOption_E_text());
        if (q.getOption_E_Image_url()!=null && !q.getOption_E_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.optEImg.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getOption_E_Image_url(), h.optEImg);
        }

        h.correct_ans.setText(q.getCorrect_ans());
        if (q.getCorrect_Ans_Image_url()!=null && !q.getCorrect_Ans_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.correct_ans_img.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getCorrect_Ans_Image_url(), h.correct_ans_img);
        }

        h.explain.setText(q.getExplaination());
        if (q.getExplanation_Image_url() !=null && !q.getExplanation_Image_url().equals("https://firebasestorage.googleapis.com/v0/b/pratiyogita-samanya-gyan.appspot.com/o/images%2Fno_image.png?alt=media&token=cfcffe48-7085-4500-8e56-86b8aea1fb1c")) {
            h.exp_img.setVisibility(View.VISIBLE);
            imageLoader.displayImage(q.getExplanation_Image_url(), h.exp_img);
        }
        if(q.getCorrect_ans().equals("A")) {
            h.optA.setBackgroundResource(R.drawable.shadow_right_button);
            h.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
        } else if(q.getCorrect_ans().equals("B")) {
            h.optB.setBackgroundResource(R.drawable.shadow_right_button);
            h.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
        }else if(q.getCorrect_ans().equals("C")) {
            h.optC.setBackgroundResource(R.drawable.shadow_right_button);
            h.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
        }else if(q.getCorrect_ans().equals("D")) {
            h.optD.setBackgroundResource(R.drawable.shadow_right_button);
            h.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optE.setBackgroundResource(R.drawable.shadow_wrong_button);
        }else if(q.getCorrect_ans().equals("E")) {
            h.optE.setBackgroundResource(R.drawable.shadow_right_button);
            h.optA.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optC.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optD.setBackgroundResource(R.drawable.shadow_wrong_button);
            h.optB.setBackgroundResource(R.drawable.shadow_wrong_button);
        }


        h.txtAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.e("StudyQuestion","OnAnsClick");
                if (h.layoutAns.isShown()) {
                    AnimationUtility.slide_up(context, h.layoutAns);
                    h.layoutAns.setVisibility(View.GONE);
                } else {
                    h.layoutAns.setVisibility(View.VISIBLE);
                    AnimationUtility.slide_down(context, h.layoutAns);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return values.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView question,correct_ans,explain,questioncount;
        public Button optA,optB,optC,optD,optE;
        public ImageView qImg,optAImg,optBImg,optCImg,optDImg,optEImg,correct_ans_img,exp_img;
        public LinearLayout layoutAns;
        public TextView txtAnswer;
        public View layout;
        public ViewHolder(View v) {
            super(v);
            layout = v;
            layoutAns = (LinearLayout)itemView.findViewById(R.id.layoutAnswer);
            layoutAns.setVisibility(View.GONE);
            questioncount = (TextView)itemView.findViewById(R.id.lblquestioncount);
            txtAnswer = (TextView)itemView.findViewById(R.id.lblAnswer) ;
            question = (TextView)itemView.findViewById(R.id.question);
            optA = (Button) itemView.findViewById(R.id.optionA);
            optB = (Button)itemView.findViewById(R.id.optionB);
            optC = (Button)itemView.findViewById(R.id.optionC);
            optD = (Button)itemView.findViewById(R.id.optionD);
            optE = (Button)itemView.findViewById(R.id.optionE);
            correct_ans = (TextView)itemView.findViewById(R.id.answer);
            explain = (TextView)itemView.findViewById(R.id.explaination);

            qImg = (ImageView) itemView.findViewById(R.id.questionimage);
            optAImg = (ImageView) itemView.findViewById(R.id.optionA_image);
            optBImg = (ImageView) itemView.findViewById(R.id.optionB_image);
            optCImg = (ImageView) itemView.findViewById(R.id.optionC_image);
            optDImg = (ImageView) itemView.findViewById(R.id.optionD_image);
            optEImg = (ImageView) itemView.findViewById(R.id.optionE_image);
            correct_ans_img = (ImageView) itemView.findViewById(R.id.answer_image);
            exp_img = (ImageView) itemView.findViewById(R.id.expl_image);

        }
    }
}
