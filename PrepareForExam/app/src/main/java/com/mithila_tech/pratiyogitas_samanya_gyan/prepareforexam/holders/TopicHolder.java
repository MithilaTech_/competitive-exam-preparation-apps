package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;

/**
 * Created by ASUS on 12/14/2017.
 */

public class TopicHolder extends RecyclerView.ViewHolder{
    private final String TAG = com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.TopicHolder.class.getSimpleName();

    public TextView categoryTitle;


    public TopicHolder(final Context context, final View itemView) {
        super(itemView);

        categoryTitle = (TextView)itemView.findViewById(R.id.topic_title);


    }
}