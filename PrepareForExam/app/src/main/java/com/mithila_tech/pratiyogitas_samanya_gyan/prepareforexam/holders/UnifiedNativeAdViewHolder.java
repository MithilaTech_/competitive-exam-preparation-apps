package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.R;

public class UnifiedNativeAdViewHolder extends BaseViewHolder {
    private UnifiedNativeAdView adView;
    public UnifiedNativeAdViewHolder(@NonNull View itemView) {
        super(itemView);
        adView = itemView.findViewById(R.id.ad_view);
        adView.setHeadlineView(itemView.findViewById(R.id.ad_headline));
        adView.setBodyView(itemView.findViewById(R.id.ad_body));
        adView.setCallToActionView(itemView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(itemView.findViewById(R.id.ad_icon));
        adView.setPriceView(itemView.findViewById(R.id.ad_price));
        adView.setStarRatingView(itemView.findViewById(R.id.ad_stars));
        adView.setStoreView(itemView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(itemView.findViewById(R.id.ad_advertiser));


    }

    @Override
    protected void clear() {

    }

    public UnifiedNativeAdView getAdView(){
        return  adView;
    }
}
