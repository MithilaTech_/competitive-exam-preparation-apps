package com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.BaseViewHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.SubjectHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.holders.UnifiedNativeAdViewHolder;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.model.Subject;
import com.mithila_tech.pratiyogitas_samanya_gyan.prepareforexam.utils.SharedPrefManager;
import com.shreyaspatil.firebase.recyclerpagination.DatabasePagingOptions;
import com.shreyaspatil.firebase.recyclerpagination.FirebaseRecyclerPagingAdapter;
import com.shreyaspatil.firebase.recyclerpagination.LoadingState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class TestSubjectActivity extends AppCompatActivity {

    private static final String TAG = TestSubjectActivity.class.getSimpleName();
    private static final int ITEMS_PER_AD=8;
    private static final int NATIVE_EXPRESS_AD_HEIGHT=  150;
    private static final String AD_UNIT_ID ="ca-app-pub-3940256099942544/2247696110";
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseRecyclerPagingAdapter recyclerViewAdapter;
     private DatabaseReference databaseReference,subjectreference;
    private List<Object> allSubjects;
    private String slanguage;
    private ProgressDialog mProgressBar;
    private int count=0;
    private boolean flag=false,subjectwise = false,istopictestseries=false,issubjecttestseries=false,iscommontestseries=false;
    private Query query;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DatabasePagingOptions<Subject> options;
    private static final int NUMBER_OF_ADS = 5;
    private AdLoader adLoader;
    private List<UnifiedNativeAd> nativeAds = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);//initializing firebase authentication object
        getSupportActionBar().setTitle("Subjects");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        if(intent!=null) {

            flag = intent.getBooleanExtra("isStudyTest",true);
            istopictestseries = intent.getBooleanExtra("istopictestseries",false);
            issubjecttestseries = intent.getBooleanExtra("issubjecttestseries",false);
            iscommontestseries = intent.getBooleanExtra("iscommontestseries",false);

        }
        mProgressBar = new ProgressDialog(TestSubjectActivity.this);
        mProgressBar.setCancelable(false);

        allSubjects= new ArrayList<Object>();

        slanguage = SharedPrefManager.getLang(TestSubjectActivity.this);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();
        databaseReference.keepSynced(true);
        databaseReference.keepSynced(false);
        subjectreference = databaseReference.child("/subjects");
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(5)
                .setPageSize(10)
                .build();

        //Initialize FirebasePagingOptions
        options = new DatabasePagingOptions.Builder<Subject>()
                .setLifecycleOwner(this)
                .setQuery(subjectreference, config, Subject.class)
                .build();
        query = subjectreference.orderByChild("slanguage").equalTo(slanguage);

        recyclerView = (RecyclerView)findViewById(R.id.sub_list);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.scrollToPosition(0);


        mProgressBar.setIndeterminate(true);
        mProgressBar.setMessage(getString(R.string.title_loading));
        mProgressBar.show();

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                HashMap<String,String> ss= (HashMap<String, String>) dataSnapshot.getValue();
                String sname=ss.get("sname");
                String slang=ss.get("slanguage");

                Subject subject = new Subject();
                subject.setSid(String.valueOf(count));
                subject.setSname(sname);
                subject.setSlanguage(slang);
                allSubjects.add(subject);
                Log.e("Sizeee","Sizeee"+allSubjects.size());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(allSubjects!=null && allSubjects.size()>0) {
                    if(mProgressBar.isShowing()) {
                    mProgressBar.dismiss();
                }
                    fillData();

                } else {
                    if(mProgressBar.isShowing()) {
                        mProgressBar.dismiss();

                    }
                    Toast.makeText(TestSubjectActivity.this, "No subjects found...", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_ad_unit_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void fillData() {
        loadNativeAds();
        recyclerViewAdapter = new FirebaseRecyclerPagingAdapter<Subject, BaseViewHolder>(
              options) {
            int counter=0;
            private final int ITEM_VIEW_TYPE =0;
            private final int UNIFIED_NATIVE_AD_TYPE =1;
            @Override
            public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                switch(viewType) {
                    case UNIFIED_NATIVE_AD_TYPE:
                        View unifiedNativeLayoutView = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.ad_unified, parent, false);

                        return new UnifiedNativeAdViewHolder(unifiedNativeLayoutView);
                    case ITEM_VIEW_TYPE:
                    default:
                        View view = LayoutInflater.from(parent.getContext())
                                    .inflate(R.layout.subject_list, parent, false);

                            return new SubjectHolder(TestSubjectActivity.this, view);
                }

            }

            @Override
            public int getItemViewType(int position) {
                if(allSubjects.get(position) instanceof  Subject)
                    return 0;
                else
                    return 1;
            }

            @Override
            public void onBindViewHolder(@NonNull final BaseViewHolder holder,
                                         int position,
                                         @NonNull Subject model) {
               // super.onBindViewHolder(holder, position, model);
                int viewType = getItemViewType(position);
                Log.e("SubjectActivity","subjects:"+allSubjects.size());
                switch(viewType) {
                    case UNIFIED_NATIVE_AD_TYPE:
                        UnifiedNativeAd unifiedNativeAd = (UnifiedNativeAd) allSubjects.get(position);
                        populateNativeAdView(unifiedNativeAd,((UnifiedNativeAdViewHolder)holder).getAdView());
                    break;
                    case ITEM_VIEW_TYPE:

                    if (position < allSubjects.size()) {
                        // Log.e("SubjectActivity","count:"+position);
                        final Subject subject = (Subject) allSubjects.get(position);
                        if (subject != null) {
                            String firstLetter = subject.getSname();
                            if (firstLetter != null && firstLetter.length() > 0) {
                                ((SubjectHolder)holder).categoryTitle.setText(firstLetter);
                            }

                        }
                        ((SubjectHolder)holder).categoryTitle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!TextUtils.isEmpty(((SubjectHolder)holder).categoryTitle.getText()) && (flag || istopictestseries)) {
                                    String taskTitle = subject.getSname();
                                    Intent i = new Intent(TestSubjectActivity.this, TopicActivity.class);
                                    i.putExtra("subjectname", taskTitle);
                                    i.putExtra("isStudyTest", flag);
                                    i.putExtra("istopictestseries", istopictestseries);
                                    i.putExtra("issubjecttestseries", issubjecttestseries);
                                    i.putExtra("iscommontestseries", iscommontestseries);
                                    TestSubjectActivity.this.startActivity(i);
                                } else if (!TextUtils.isEmpty(((SubjectHolder)holder).categoryTitle.getText()) && (issubjecttestseries)) {
                                    String taskTitle = subject.getSname();
                                    Intent i = new Intent(TestSubjectActivity.this, AvailableTestSeries.class);
                                    i.putExtra("subjectname", taskTitle);
                                    i.putExtra("istopictestseries", istopictestseries);
                                    i.putExtra("issubjecttestseries", issubjecttestseries);
                                    i.putExtra("iscommontestseries", iscommontestseries);
                                    TestSubjectActivity.this.startActivity(i);
                                } else {
                                    Toast.makeText(TestSubjectActivity.this, "No topics found...", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }
                        ((SubjectHolder)holder).setItem(model);
                }

            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                case LOADING_INITIAL:
                case LOADING_MORE:
                    // Do your loading animation
                    mSwipeRefreshLayout.setRefreshing(true);
                    break;

                case LOADED:
                    // Stop Animation
                    mSwipeRefreshLayout.setRefreshing(false);
                    break;

                case FINISHED:
                    //Reached end of Data set
                    mSwipeRefreshLayout.setRefreshing(false);
                    break;

                case ERROR:
                    retry();
                    break;
            }
        }

        @Override
        protected void onError(@NonNull DatabaseError databaseError) {
            super.onError(databaseError);
            mSwipeRefreshLayout.setRefreshing(false);
            databaseError.toException().printStackTrace();
        }

            private void populateNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView adView) {
                ((TextView)adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
                ((TextView)adView.getBodyView()).setText(unifiedNativeAd.getBody());
                ((TextView)adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());

                NativeAd.Image icon = unifiedNativeAd.getIcon();
                if(icon==null){
                    adView.getIconView().setVisibility(View.INVISIBLE);
                } else {
                    ((ImageView)adView.getIconView()).setImageDrawable(icon.getDrawable());
                    adView.getIconView().setVisibility(View.VISIBLE);
                }

                if(unifiedNativeAd.getPrice()==null) {
                    adView.getPriceView().setVisibility(View.INVISIBLE);
                } else {
                    adView.getPriceView().setVisibility(View.VISIBLE);
                    ((TextView)adView.getPriceView()).setText(unifiedNativeAd.getPrice());
                }

                if(unifiedNativeAd.getStore()==null) {
                    adView.getStoreView().setVisibility(View.INVISIBLE);
                } else {
                    adView.getStoreView().setVisibility(View.VISIBLE);
                    ((TextView)adView.getStoreView()).setText(unifiedNativeAd.getStore());
                }

                if(unifiedNativeAd.getStarRating()==null) {
                    adView.getStarRatingView().setVisibility(View.INVISIBLE);
                } else {
                    adView.getStarRatingView().setVisibility(View.VISIBLE);
                    ((RatingBar)adView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
                }

                if(unifiedNativeAd.getAdvertiser()==null) {
                    adView.getAdvertiserView().setVisibility(View.INVISIBLE);
                } else {
                    adView.getAdvertiserView().setVisibility(View.VISIBLE);
                    ((TextView)adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
                }

                adView.setNativeAd(unifiedNativeAd);
            }

            @Override
            public int getItemCount() {
                if(allSubjects!= null && allSubjects.size()>0)
                return allSubjects.size();
                else
                    return 0;
            }
        };

        recyclerView.setLayoutManager(linearLayoutManager);
      //  recyclerView.setItemViewCacheSize(10);
       // recyclerView.setHasFixedSize(true);
        recyclerView.setVisibility(View.VISIBLE);

        recyclerView.setAdapter(recyclerViewAdapter);
        //Set listener to SwipeRefreshLayout for refresh action
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerViewAdapter.refresh();
            }
        });

    }



   /* public void addNativeExpressAds() {
        for (int i=0;i<allSubjects.size();i++) {
            final NativeExpressAdView adView = new NativeExpressAdView(SubjectActivity.this);
            allSubjects.add(i,adView);
        }
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(recyclerViewAdapter!=null) {
            recyclerViewAdapter.stopListening();
        }
    }

    private void loadNativeAds() {
        AdLoader.Builder builder = new AdLoader.Builder(TestSubjectActivity.this    ,getString(R.string.native_ads_id));
        adLoader = builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
            @Override
            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                nativeAds.add(unifiedNativeAd);
                if(!adLoader.isLoading()) {
                    insertAdsInMenuItem();
                }
            }
        }).withAdListener(new AdListener(){
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if(!adLoader.isLoading()) {
                    insertAdsInMenuItem();
                }
            }
        }).build();
        adLoader.loadAds(new AdRequest.Builder().build(),NUMBER_OF_ADS);
    }

    private void insertAdsInMenuItem() {
        if(nativeAds.size()<=0) {
            return;
        }

        int offset = (allSubjects.size()/nativeAds.size()+1);
        int index =0;
        for (UnifiedNativeAd ad: nativeAds) {
            allSubjects.add(index,ad);
            index = index+offset;
        }
    }

}
